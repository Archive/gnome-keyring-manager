=================
Version 2.??.?
=================

	* #337908: Use po/LINGUAS instead of ALL_LINGUAS in configure.ac
	  (Przemysrlaw Grzegorczyk)
	* #328039: Fixed .desktop definition for freedesktop compliance
	  (Germ�n Po�-Caama�o)
	* #335041: Added MAINTAINERS file (Germ�n Po�-Caama�o)
	* #336077: Port to GOption API (Sebastien Bacher)
	* Removed unused files because of UI changes (Behdad Esfahbod)
	* #337310: Get rid of m4 directory that does not exist (Behdad Esfahbod)
	* Implement for FreeBSD (Dan Williams)
	* Deal with possibly NULL application path (Dan Williams)
	* #327946: Updates for gnome-doc-utils (Brent Smith)
	* Don't set edited keyring if not passed a keyring name (Matthias Clasen)
	* Don't open 'default' keyring, but defer setting the edited 
	  keyring until keyrings are loaded (Matthias Clasen)
	* Use the first item in the keyring list if there is no currently
	  edited keyring, use the first item in the keyring list (Matthias Clasen)
	* Connect keyring treeview row-changed signal (Matthias Clasen)
	* Remove "invisible_char" items because they override the GTK default,
	  which is now pretty, with ugly obfuscation characters (Dan Williams)
	* #326273: Don't use "GNOME" in about dialog title.
	* gkm_about_dialog_new renamed to gkm_show_about_dialog 
	  (Christian Persch)
	* Fix network attribute mishandling when a default attribute 
	  isn't present in the attribute list (Dan Williams)
	* #336692: Allow ports higher than 100 (Dan Williams)
	* Fix "unused result" error on 
	  gkm_application_window_destroyed_callback (Christian Persch)

New and updated Translations:
	* ka (Vladimer Sichinava)
	* nn (�~Esmund Skjæveland)
	* id (Ahmad Riza H Nst)
	* sv (Daniel Nylander)
	* fr (Christophe Bliard)
	* dz (Pema Geyleg)
	* de (Hendrik Richter)
	* lv (Raivis Dejus)
	* ta (I.Felix)
	* bn_IN (Runa Bhattacharjee)
	* es (Francisco Javier F. Serrador)
	* mk (Jovan Naumovski)
	* sl (Jovan Naumovski)

Removed translations:
	* no (Kjartan Maraas)

=================
Version 2.11.2
=================

	* Misc fixes by Nickolay V. Shmyrev

=================
Version 2.11.1
=================

	* Cool keyrings sidebar
	* Switch to GtkUIManager (Claudio Saavedra)
	* Key ACL viewing and modification
	* Edit key attributes and secrets

New and updated Translations:

	* Alexander Shopov (bg), Runa Bhattacharjee (bn), Xavier Conde Rueda (ca),
	  Miloslav Trmac (cs), Martin Willemoes Hansen (da), Hendrik Richter (de),
	  Kostas Papadimas (el), Adam Weinberger (en_CA), David Lodge (en_GB), 
	  Francisco Javier F. Serrador (es), Tommi Vainikainen (fi), Thierry Moisan (fr),
	  Gabor Kelemen (hu), Francesco Marletta (it), Takeshi AIHANA (ja), Young-Ho Cha (ko),
	  Žygimantas Beručka (lt), Kjartan Maraas (nb), Tino Mienen (nl), Kjartan Maraas (no), 
	  GNOME PL Team (pl), Raphael Higino (pt_BR), Duarte Loreto (pt), Steve Murphy (rw), 
	  Elian Myftiu (sq), Christian Rose (sv), Maxim Dziumanenko (uk), Funda Wang (zh_CN)

=================
Version 2.11.0
=================

	* Removed the suck (James Bowes)
	* Shiny New UI! (courtesy James Cape)
	* Beginnings of documentation (James Bowes)

New and updated Translations:

	* cs(Miloslav Trmac), da(Martin Willemoes Hansen), 
	  de(Hendrik Brandt), el(Kostas Papadimas), en_CA(Adam Weinberger),
	  en_GB(David Lodge), es(Francisco Javier F. Serrador),
	  fr(Christophe Merlet), it(Alessio Frusciante), ja(Takeshi AIHANA),
	  lt(Žygimantas Beručka), nl(Tino Mienen), sq(Elian Myftui)

================
Version 0.0.4
================

	* Use pixbufs to show item types.
	* Show correct secret after deletion (Caleb Groom)
	* Fixed sensitivity update bugs (Caleb Groom)
	* Select the next attribute/ac after deletion (James Bowes)
	* New sorting stuff (James Bowes)
	* Fix ugly bug when right clicking (Chris Kelso)
	* Lot of string fixed (Christian Rose)
	* Lot of translation updates (GNOME i18n Team)


===============
Version 0.0.3
===============

	* ACL viewing and editing
	* Unsensitive widgets when there is no selection (Caleb Groom and
	  Claudio Saavedra)
	* Use a stock icon for the menu entry	


==============
Version 0.0.2
==============

	Real Application developed.


==============
Version 0.0.1
==============

	First skeleton and ideas.
