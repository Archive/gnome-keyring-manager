dnl
dnl Copyright (C) 2004 Fernando Herrera <fherrera@onirica.com>
dnl Copyright (C) 2004 Mariano Suárez-Alvarez <mariano@gnome.org>
dnl Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl 
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
dnl

AC_PREREQ(2.53)
AC_INIT(gnome-keyring-manager)
AM_INIT_AUTOMAKE(gnome-keyring-manager, 2.20.0)
AC_CONFIG_SRCDIR(src/gnome-keyring-manager.c)
AM_CONFIG_HEADER(config.h)

GNOME_DOC_INIT

AM_MAINTAINER_MODE

AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
AM_PROG_LIBTOOL
IT_PROG_INTLTOOL([0.35.0])
AC_PATH_XTRA 
GNOME_COMPILE_WARNINGS

GTK_REQUIRED=2.6.0
LIBGLADE_REQUIRED=2.0.0
LIBGNOMEUI_REQUIRED=2.6.0
GNOME_KEYRING_REQUIRED=0.3.2

AC_PATH_PROG(GCONFTOOL, [gconftool-2])
AM_GCONF_SOURCE_2

GNOME_DOC_INIT

PKG_CHECK_MODULES(GNOME_KEYRING_MANAGER,
                  gtk+-2.0 >= $GTK_REQUIRED             		
                  libglade-2.0 >= $LIBGLADE_REQUIRED       	
                  libgnomeui-2.0 >= $LIBGNOMEUI_REQUIRED
		  gnome-keyring-1 >= $GNOME_KEYRING_REQUIRED)

AC_SUBST(GNOME_KEYRING_MANAGER_CFLAGS)
AC_SUBST(GNOME_KEYRING_MANAGER_LIBS)

GNOME_KEYRING_MANAGER_CFLAGS="$GNOME_KEYRING_MANAGER_CFLAGS -Wall -W -Wno-format-y2k"

AM_GLIB_GNU_GETTEXT

GETTEXT_PACKAGE=gnome-keyring-manager
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Define the gettext package name.])

AC_OUTPUT([ 
Makefile
data/gnome-keyring-manager.desktop.in
data/Makefile
docs/Makefile
src/Makefile
po/Makefile.in
])
