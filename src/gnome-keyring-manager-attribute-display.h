/*
   Copyright (C) 2004 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GNOME_KEYRING_MANAGER_ATTRIBUTE_DISPLAY_H
#define GNOME_KEYRING_MANAGER_ATTRIBUTE_DISPLAY_H

#include <config.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GKM_TYPE_ATTRIBUTE_DISPLAY            (gkm_attribute_display_get_type ())
#define GKM_ATTRIBUTE_DISPLAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GKM_TYPE_ATTRIBUTE_DISPLAY, GKMAttributeDisplay))
#define GKM_ATTRIBUTE_DISPLAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GKM_TYPE_ATTRIBUTE_DISPLAY, GKMAttributeDisplayClass))
#define GKM_IS_ATTRIBUTE_DISPLAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GKM_TYPE_ATTRIBUTE_DISPLAY))
#define GKM_IS_ATTRIBUTE_DISPLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GKM_TYPE_ATTRIBUTE_DISPLAY))
#define GKM_ATTRIBUTE_DISPLAY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GKM_TYPE_ATTRIBUTE_DISPLAY, GKMAttributeDisplayClass))

typedef struct _GKMAttributeDisplay         GKMAttributeDisplay;
typedef struct _GKMAttributeDisplayClass    GKMAttributeDisplayClass;
typedef struct _GKMAttributeDisplayPrivate  GKMAttributeDisplayPrivate;

struct _GKMAttributeDisplay
{
  GtkVBox parent;

  GKMAttributeDisplayPrivate *priv;
};

struct _GKMAttributeDisplayClass
{
  GtkVBoxClass parent;

  void (* secret_changed)    (GKMAttributeDisplay *display,
			      gchar               *new_secret);

  void (* attribute_changed) (GKMAttributeDisplay *display,
			      gpointer             new_attributes);
};

GType gkm_attribute_display_get_type (void);

GtkWidget                 *gkm_attribute_display_new            (void);

void                       gkm_attribute_display_set_secret     (GKMAttributeDisplay *self,
							         const gchar         *secret);
gchar                     *gkm_attribute_display_get_secret     (GKMAttributeDisplay *self);

void                       gkm_attribute_display_set_item_type  (GKMAttributeDisplay *display,
								 GnomeKeyringItemType item_type);
GnomeKeyringItemType       gkm_attribute_display_get_item_type  (GKMAttributeDisplay *display);

void                       gkm_attribute_display_set_attributes (GKMAttributeDisplay       *display,
			                                         GnomeKeyringAttributeList *attributes);
GnomeKeyringAttributeList *gkm_attribute_display_get_attributes (GKMAttributeDisplay *display);

G_END_DECLS

#endif
