/* 
   Copyright (C) 2004 Fernando Herrera <fherrera@onirica.com>
   Copyright (C) 2004 Mariano Suárez-Alvarez <mariano@gnome.org>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <config.h>

#include <stdlib.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libgnomeui/gnome-ui-init.h>

#include "gnome-keyring-manager.h"
#include "gnome-keyring-manager-util.h"
#include "gnome-keyring-manager-main-ui.h"

#define GNOME_KEYRING_ICON "stock_keyring"

/********************************************************************
 * The application
 */

typedef struct _GKMApplication GKMApplication;

struct _GKMApplication
{
  GtkWidget *about;
  GSList    *managers;
};

enum
{
  MANAGER_WINDOW,
  ABOUT_WINDOW
};

static GKMApplication *application = NULL;

static void gkm_application_window_destroyed_callback (GObject *window, gpointer user_data);

void
gkm_application_init (void)
{
  g_assert (application == NULL);

  gkm_register_icons ();

  application = g_new0 (GKMApplication, 1);

  application->about = NULL;
  application->managers = NULL;
}

void
gkm_application_open_keyring_manager (void)
{
  GObject *manager;

  g_assert (application != NULL);

  manager = gkm_main_ui_new ();
  g_signal_connect (manager, "destroy", 
		    G_CALLBACK (gkm_application_window_destroyed_callback), 
		    GINT_TO_POINTER (MANAGER_WINDOW));

  application->managers = g_slist_append (application->managers, manager);
}

void
gkm_application_quit (void)
{
  g_assert (g_slist_length (application->managers) == 0);

  /*FIXME: is this all? */
  if (application->about != NULL)
    {
      gtk_widget_destroy (application->about);
    }
  g_free (application);
  application = NULL;

  gtk_main_quit ();
}

static void
gkm_application_window_destroyed_callback (GObject *window, gpointer user_data)
{
  switch (GPOINTER_TO_INT (user_data))
    {
    case MANAGER_WINDOW:
      application->managers = g_slist_remove (application->managers, window);
      break;
    case ABOUT_WINDOW:
      application->about = NULL;
      break;
    default:
      g_assert_not_reached ();
    }

  if (g_slist_length (application->managers) == 0)
    {
      gkm_application_quit ();
    }
}

/********************************************************************
 * Getting things going
 */

static const GOptionEntry options[] = {
        { NULL}
};

int
main (int argc, char **argv)
{
  GOptionContext *context;

  bindtextdomain (GETTEXT_PACKAGE, GNOME_KEYRING_MANAGER_LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
  
  g_set_application_name (_("Keyring Manager"));

  context = g_option_context_new (NULL);
  g_option_context_add_main_entries (context, options, GETTEXT_PACKAGE);
  
  gnome_program_init (PACKAGE, VERSION,
                      LIBGNOMEUI_MODULE, argc, argv,
                      GNOME_PARAM_GOPTION_CONTEXT, context,
		      GNOME_PROGRAM_STANDARD_PROPERTIES,
                      GNOME_PARAM_NONE);

  gtk_window_set_default_icon_name ("stock_keyring");

  gkm_application_init ();

  gkm_application_open_keyring_manager ();

  gtk_main ();
  
  return 0;
}
