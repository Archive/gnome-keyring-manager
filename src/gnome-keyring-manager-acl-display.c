/* 
   Copyright (C) 2005 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2005 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gnome-keyring.h>
#include <glade/glade.h>

#include "gnome-keyring-manager-util.h"
#include "gnome-keyring-manager-acl-display.h"

#define GLADE_ROOT "acl-display_vpaned"

struct _GKMAclDisplayPrivate
{
  GnomeKeyringItemType item_type;

  GtkLabel *path_label;

  GtkToggleButton *read_checkbutton;
  GtkToggleButton *write_checkbutton;
  GtkToggleButton *delete_checkbutton;

  guint32 read_signal_id;
  guint32 write_signal_id;
  guint32 delete_signal_id;

  GtkTreeView  *acl_treeview;
  GtkListStore *acl_store;

  GnomeKeyringAccessControl *gkm_ac;
};

typedef enum _GKMAclDisplayColumn GKMAclDisplayColumn;

enum _GKMAclDisplayColumn
  {
    AC_COLUMN_NAME = 0,
    AC_COLUMN_PATH,
    AC_COLUMN_READ,
    AC_COLUMN_WRITE,
    AC_COLUMN_DELETE,
    NUM_AC_COLUMNS
  };

enum 
  {
    ACL_CHANGED,
    LAST_SIGNAL
  };

static guint acl_display_signals[LAST_SIGNAL] = { 0 };

static void gkm_acl_display_finalize   (GObject *object);
static void gkm_acl_display_destroy    (GtkObject *object);

static void gkm_acl_display_setup_acl_tree (GKMAclDisplay *self);

static void on_acl_treeview_selection_changed (GtkTreeSelection *selection, 
					       GKMAclDisplay    *self);

static void on_read_checkbutton_toggled   (GtkWidget     *widget,
					   GKMAclDisplay *self);
static void on_write_checkbutton_toggled  (GtkWidget     *widget,
					   GKMAclDisplay *self);
static void on_delete_checkbutton_toggled (GtkWidget     *widget,
					   GKMAclDisplay *self);

G_DEFINE_TYPE (GKMAclDisplay, gkm_acl_display, GTK_TYPE_VBOX);

static void
gkm_acl_display_class_init (GKMAclDisplayClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);

  gobject_class->finalize = gkm_acl_display_finalize;

  object_class->destroy = gkm_acl_display_destroy;

  acl_display_signals[ACL_CHANGED] =
    g_signal_new ("acl-changed",
		  G_OBJECT_CLASS_TYPE (gobject_class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GKMAclDisplayClass, acl_changed),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1,
		  G_TYPE_POINTER);
}

static void
gkm_acl_display_init (GKMAclDisplay *self)
{
  GtkWidget        *widget;
  GtkTreeSelection *selection;
  GladeXML         *xml;

  self->priv = g_new0 (GKMAclDisplayPrivate, 1);
  
  xml = glade_xml_new (GLADEDIR GLADEFILE,
		       GLADE_ROOT, NULL);

  if(!xml) 
    {
      g_warning ("Unable to load the glade file. Your install is broken.");
      exit (1);
    }

  widget = glade_xml_get_widget (xml, GLADE_ROOT);

  gtk_container_add (GTK_CONTAINER (self), widget);

  /* Get refs for the required widgets */

  widget = glade_xml_get_widget (xml, "acl_treeview");
  self->priv->acl_treeview = GTK_TREE_VIEW (widget);

  selection = gtk_tree_view_get_selection (self->priv->acl_treeview);
  g_signal_connect (G_OBJECT (selection),
		    "changed",
		    G_CALLBACK (on_acl_treeview_selection_changed), 
		    self);

  widget = glade_xml_get_widget (xml, "path_label");
  self->priv->path_label = GTK_LABEL (widget);

  widget = glade_xml_get_widget (xml, "read_checkbutton");
  self->priv->read_checkbutton = GTK_TOGGLE_BUTTON (widget);
  self->priv->read_signal_id = 
    g_signal_connect (widget,
		      "toggled",
		      G_CALLBACK (on_read_checkbutton_toggled),
		      self);

  widget = glade_xml_get_widget (xml, "write_checkbutton");
  self->priv->write_checkbutton = GTK_TOGGLE_BUTTON (widget);
  self->priv->write_signal_id = 
    g_signal_connect (widget,
		      "toggled",
		      G_CALLBACK (on_write_checkbutton_toggled),
		      self);

  widget = glade_xml_get_widget (xml, "delete_checkbutton");
  self->priv->delete_checkbutton = GTK_TOGGLE_BUTTON (widget);
  self->priv->delete_signal_id = 
    g_signal_connect (widget,
		      "toggled",
		      G_CALLBACK (on_delete_checkbutton_toggled),
		      self);

  gkm_acl_display_setup_acl_tree (self);

  gtk_widget_show_all (GTK_WIDGET (self));
  
  g_object_unref (xml);
}

static void
gkm_acl_display_finalize (GObject *object)
{
  GKMAclDisplay *display;

  g_return_if_fail (GKM_IS_ACL_DISPLAY (object));

  display = GKM_ACL_DISPLAY (object);

  if (display->priv->gkm_ac != NULL)
    {
      gnome_keyring_access_control_free (display->priv->gkm_ac);
    }

  g_free (display->priv);

  G_OBJECT_CLASS (gkm_acl_display_parent_class)->finalize (object);
}

static void
gkm_acl_display_destroy (GtkObject *object)
{
  GKMAclDisplay *display;

  g_return_if_fail (GKM_IS_ACL_DISPLAY (object));

  display = GKM_ACL_DISPLAY (object);

  GTK_OBJECT_CLASS (gkm_acl_display_parent_class)->destroy (GTK_OBJECT (display));
}

GtkWidget *
gkm_acl_display_new (void)
{
  GKMAclDisplay *display;

  display = g_object_new (GKM_TYPE_ACL_DISPLAY, NULL);

  return GTK_WIDGET (display);
}

/***************************************************
 * Set up the ACL Tree model/view.
 */

static void
gkm_acl_display_setup_acl_tree (GKMAclDisplay *self)
{
  GtkTreeViewColumn   *column;
  GtkCellRenderer     *renderer;
  
  g_return_if_fail (GKM_IS_ACL_DISPLAY (self));
  g_assert (self->priv->acl_store == NULL);
  
  self->priv->acl_store = 
    gtk_list_store_new (NUM_AC_COLUMNS, 
			/* AC_COLUMN_NAME  */    G_TYPE_STRING, 
			/* AC_COLUMN_PATH */     G_TYPE_STRING,
			/* AC_COLUMN_READ */     G_TYPE_BOOLEAN,
			/* AC_COLUMN_WRITE */    G_TYPE_BOOLEAN,
			/* AC_COLUMN_DELETE */   G_TYPE_BOOLEAN);
  
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Name"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->acl_treeview), 
			       column);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_add_attribute (column, renderer, "text", 
				      AC_COLUMN_NAME);
  gtk_tree_view_column_set_sort_column_id (column, AC_COLUMN_NAME);
  g_object_set (G_OBJECT (renderer), "editable", FALSE, NULL);
 
  gtk_tree_view_set_model (GTK_TREE_VIEW (self->priv->acl_treeview), 
			   GTK_TREE_MODEL (self->priv->acl_store));
  g_object_unref (G_OBJECT (self->priv->acl_store));
}

static void 
on_acl_treeview_selection_changed (GtkTreeSelection *selection, 
				   GKMAclDisplay    *self)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;
  gboolean      sensitive;
  gboolean      read;
  gboolean      write;
  gboolean      delete;
  gchar        *path;

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gtk_tree_model_get (model, &iter,
			  AC_COLUMN_PATH,   &path,
			  AC_COLUMN_READ,   &read,
			  AC_COLUMN_WRITE,  &write,
			  AC_COLUMN_DELETE, &delete, -1);

      sensitive = TRUE;
    }
  else
    {
      path = g_strdup ("");

      sensitive = FALSE;
      read      = FALSE;
      write     = FALSE;
      delete    = FALSE;
    }

  gtk_label_set_text (self->priv->path_label, path);
  g_free (path);

  g_signal_handler_block (self->priv->read_checkbutton, 
			  self->priv->read_signal_id);
  g_signal_handler_block (self->priv->write_checkbutton, 
			  self->priv->write_signal_id);
  g_signal_handler_block (self->priv->delete_checkbutton, 
			  self->priv->delete_signal_id);

  gtk_toggle_button_set_active (self->priv->read_checkbutton,   read);
  gtk_toggle_button_set_active (self->priv->write_checkbutton,  write);
  gtk_toggle_button_set_active (self->priv->delete_checkbutton, delete);

  g_signal_handler_unblock (self->priv->read_checkbutton, 
			    self->priv->read_signal_id);
  g_signal_handler_unblock (self->priv->write_checkbutton, 
			    self->priv->write_signal_id);
  g_signal_handler_unblock (self->priv->delete_checkbutton, 
			    self->priv->delete_signal_id);


  gtk_widget_set_sensitive (GTK_WIDGET (self->priv->read_checkbutton),
			    sensitive);
  gtk_widget_set_sensitive (GTK_WIDGET (self->priv->write_checkbutton),
			    sensitive);
  gtk_widget_set_sensitive (GTK_WIDGET (self->priv->delete_checkbutton),
			    sensitive);
}

/*****************************************************
 * Checkbutton toggling callbacks.
 */

static void
checkbutton_toggled (GKMAclDisplay       *self,
		     GKMAclDisplayColumn  column,
		     GtkWidget           *toggle)
{
  GtkTreeSelection *selection;
  GtkTreeModel     *model;
  GtkTreeIter       iter;
  
  selection = gtk_tree_view_get_selection (self->priv->acl_treeview);
  
  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      GList    *acl_list;
      gboolean  active;
      
      active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
      
      gtk_list_store_set (GTK_LIST_STORE (model), &iter, 
			  column, active, -1);

      acl_list = gkm_acl_display_get_acl (self);
      g_signal_emit (self,
		     acl_display_signals[ACL_CHANGED],
		     0, acl_list);
    }
}


static void
on_read_checkbutton_toggled (GtkWidget     *toggle,
			     GKMAclDisplay *self)
{
  checkbutton_toggled (self, AC_COLUMN_READ, toggle);
}

static void
on_write_checkbutton_toggled (GtkWidget     *toggle,
			      GKMAclDisplay *self)
{
  checkbutton_toggled (self, AC_COLUMN_WRITE, toggle);
}

static void
on_delete_checkbutton_toggled (GtkWidget     *toggle,
			       GKMAclDisplay *self)
{
  checkbutton_toggled (self, AC_COLUMN_DELETE, toggle);
}

void
gkm_acl_display_set_acl (GKMAclDisplay *self,
			 GList         *acl)
{
  GList *tmp;
  const char *app_path;

  g_return_if_fail (GKM_IS_ACL_DISPLAY (self));

  if (self->priv->gkm_ac != NULL)
    {
      gnome_keyring_access_control_free (self->priv->gkm_ac);
    }

  self->priv->gkm_ac = NULL;

  gtk_list_store_clear (self->priv->acl_store);

  app_path = gkm_get_application_path ();

  for (tmp = acl; tmp; tmp = tmp->next)
    {
      GnomeKeyringAccessControl *ac;
      GnomeKeyringAccessType at;
      GtkTreeIter iter;
      char *name, *path;

      ac = (GnomeKeyringAccessControl*) tmp->data;
      name = gnome_keyring_item_ac_get_display_name (ac);
      path = gnome_keyring_item_ac_get_path_name (ac);
 
      /* We don't want to include ourself in the list. */
      if (app_path && path && g_str_equal (path, app_path))
	{
	  self->priv->gkm_ac = gnome_keyring_access_control_copy (ac);
	}
      else
	{
	  at = gnome_keyring_item_ac_get_access_type (ac);

	  gtk_list_store_append (self->priv->acl_store, &iter);
	  gtk_list_store_set (self->priv->acl_store, &iter, 
			      AC_COLUMN_NAME, name,
			      AC_COLUMN_PATH, path,
			      AC_COLUMN_READ, (at & GNOME_KEYRING_ACCESS_READ),
			      AC_COLUMN_WRITE, (at & GNOME_KEYRING_ACCESS_WRITE),
			      AC_COLUMN_DELETE, (at & GNOME_KEYRING_ACCESS_REMOVE),
			      -1);
	}

      g_free (name);
      g_free (path);
    }
}

static gboolean
add_ac_to_list (GtkTreeModel  *model,
		GtkTreePath   *treepath G_GNUC_UNUSED,
		GtkTreeIter   *iter,
		GList 	     **acl)
{
  gchar    *name;
  gchar    *path;
  gboolean  read; 
  gboolean  write;
  gboolean  remove;

  GnomeKeyringApplicationRef *ref;
  GnomeKeyringAccessControl  *ac;
  GnomeKeyringAccessType      perms;

  perms = 0;

  gtk_tree_model_get (model, iter,
		      AC_COLUMN_NAME, &name,
		      AC_COLUMN_PATH, &path,
		      AC_COLUMN_READ, &read,
		      AC_COLUMN_WRITE, &write,
		      AC_COLUMN_DELETE, &remove,
		      -1);

  ref = gnome_keyring_application_ref_new ();
  if (read)
    {
      perms |= GNOME_KEYRING_ACCESS_READ;
    }
  if (write)
    {
      perms |= GNOME_KEYRING_ACCESS_WRITE;
    }
  if (remove)
    {
      perms |= GNOME_KEYRING_ACCESS_REMOVE;
    }
 
  ac = gnome_keyring_access_control_new  (ref, perms);
  gnome_keyring_item_ac_set_display_name (ac, name);
  gnome_keyring_item_ac_set_path_name    (ac, path);

  *acl = g_list_prepend (*acl, ac);

  return FALSE;
}

GList*
gkm_acl_display_get_acl (GKMAclDisplay *self)
{
  GList *acl_list;

  g_return_val_if_fail (GKM_IS_ACL_DISPLAY (self), NULL);

  acl_list = NULL;

  gtk_tree_model_foreach (GTK_TREE_MODEL (self->priv->acl_store),
			  (GtkTreeModelForeachFunc)add_ac_to_list,
			  &acl_list);

  /* Re-add our ac */
  if (self->priv->gkm_ac)
    acl_list = g_list_prepend (acl_list, self->priv->gkm_ac);

  /* Reverse the list so elements stay in the original order. */
  return g_list_reverse (acl_list);
}

/* Glade custom widget init function */
GtkWidget *
gkm_acl_display_glade_new (gchar *widget_name G_GNUC_UNUSED, 
			   gchar *string1     G_GNUC_UNUSED, 
			   gchar *string2     G_GNUC_UNUSED,
			   gint   int1        G_GNUC_UNUSED, 
			   gint   int2        G_GNUC_UNUSED)
{
  return gkm_acl_display_new ();
}
