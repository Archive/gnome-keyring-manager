/*
   Copyright (C) 2005 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2005 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GNOME_KEYRING_MANAGER_ACL_DISPLAY_H
#define GNOME_KEYRING_MANAGER_ACL_DISPLAY_H

#include <config.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GKM_TYPE_ACL_DISPLAY            (gkm_acl_display_get_type ())
#define GKM_ACL_DISPLAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GKM_TYPE_ACL_DISPLAY, GKMAclDisplay))
#define GKM_ACL_DISPLAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GKM_TYPE_ACL_DISPLAY, GKMAclDisplayClass))
#define GKM_IS_ACL_DISPLAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GKM_TYPE_ACL_DISPLAY))
#define GKM_IS_ACL_DISPLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GKM_TYPE_ACL_DISPLAY))
#define GKM_ACL_DISPLAY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GKM_TYPE_ACL_DISPLAY, GKMAclDisplayClass))

typedef struct _GKMAclDisplay         GKMAclDisplay;
typedef struct _GKMAclDisplayClass    GKMAclDisplayClass;
typedef struct _GKMAclDisplayPrivate  GKMAclDisplayPrivate;

struct _GKMAclDisplay
{
  GtkVBox parent;

  GKMAclDisplayPrivate *priv;
};

struct _GKMAclDisplayClass
{
  GtkVBoxClass parent;

  void (* secret_changed)    (GKMAclDisplay *display,
			      gchar         *new_secret);

  void (* acl_changed) (GKMAclDisplay *display,
			gpointer       new_acl);
};

GType gkm_acl_display_get_type (void);

GtkWidget *gkm_acl_display_new (void);

void   gkm_acl_display_set_acl (GKMAclDisplay *display,
				GList         *acl);
GList *gkm_acl_display_get_acl (GKMAclDisplay *display);

G_END_DECLS

#endif
