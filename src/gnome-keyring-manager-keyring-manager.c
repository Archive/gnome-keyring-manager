/*
   Copyright (C) 2004 Fernando Herrera <fherrera@onirica.com>
   Copyright (C) 2004 Mariano Suárez-Alvarez <mariano@gnome.org>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtktreemodel.h>
#include <gnome-keyring.h>

#include "gnome-keyring-manager.h"
#include "gnome-keyring-manager-util.h"
#include "gnome-keyring-manager-keyring-editor.h"
#include "gnome-keyring-manager-keyring-manager.h"

struct _GKMKeyringManagerPrivate
{
  GtkListStore *keyrings;
  char *default_keyring;
};

static void gkm_keyring_manager_finalize   (GObject *object);

static void gkm_keyring_manager_initialize_tree_model (GKMKeyringManager *manager);

static void gkm_keyring_manager_update_keyrings (GKMKeyringManager *self);
static void gkm_keyring_manager_update_keyring_info (GKMKeyringManager *self,
						     const char *keyring, 
						     GtkTreeIter *iter);
static void gkm_keyring_manager_update_default_keyring (GKMKeyringManager *self);

G_DEFINE_TYPE (GKMKeyringManager, gkm_keyring_manager, G_TYPE_OBJECT);

/********************************************************************
 * GKMKeyringManager and GKMKeyringManagerClass
 */

static void
gkm_keyring_manager_class_init (GKMKeyringManagerClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  gobject_class->finalize = gkm_keyring_manager_finalize;
}

static void
gkm_keyring_manager_init (GKMKeyringManager *manager)
{
  manager->priv = g_new0 (GKMKeyringManagerPrivate, 1);

  gkm_keyring_manager_initialize_tree_model (manager);
 
  gkm_keyring_manager_update_keyrings (manager); 
}

static void
gkm_keyring_manager_finalize (GObject *object)
{
  GKMKeyringManager *manager;

  g_return_if_fail (GKM_IS_KEYRING_MANAGER (object));

  manager = GKM_KEYRING_MANAGER (object);

  g_free (manager->priv->default_keyring);

  g_free (manager->priv);

  G_OBJECT_CLASS (gkm_keyring_manager_parent_class)->finalize (object);
}

GObject *
gkm_keyring_manager_new (void)
{
  GKMKeyringManager *manager;

  manager = g_object_new (GKM_TYPE_KEYRING_MANAGER, NULL);

  return G_OBJECT (manager);
}

static gint
compare_keyring_lock_status (GtkTreeModel *model,
			     GtkTreeIter  *a,
			     GtkTreeIter  *b,
			     gpointer      data)
{
  gint   sortcol;
  gint   ret;
  gchar *string1;
  gchar *string2;

  sortcol = GPOINTER_TO_INT (data);
  ret = 0;
 
  gtk_tree_model_get (model, a, sortcol, &string1, -1);
  gtk_tree_model_get (model, b, sortcol, &string2, -1);
  
  /* Put Unlocked keyrings first. */
  if (strcmp (string1, string2) != 0)
    {
      ret = (strcmp (string1, UNLOCKED_ICON) == 0) ? -1 : 1;
    }
  
  g_free (string1);
  g_free (string2);
  
  return ret;
}

static void
gkm_keyring_manager_initialize_tree_model (GKMKeyringManager *manager)
{
  g_return_if_fail (GKM_IS_KEYRING_MANAGER (manager));
  g_assert (manager->priv->keyrings == NULL);

  manager->priv->keyrings = gtk_list_store_new (NUM_MANAGER_COLUMNS, 
                                                G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN, 
                                                G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT,
                                                G_TYPE_BOOLEAN);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (manager->priv->keyrings),
  				   MANAGER_COLUMN_KEYRING,
				   (GtkTreeIterCompareFunc) tree_model_compare_strings,
				   GINT_TO_POINTER (MANAGER_COLUMN_KEYRING), NULL);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (manager->priv->keyrings),
  				   MANAGER_COLUMN_LOCK,
				   (GtkTreeIterCompareFunc) compare_keyring_lock_status,
				   GINT_TO_POINTER (MANAGER_COLUMN_LOCK), NULL);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (manager->priv->keyrings),
  				   MANAGER_COLUMN_LOCK_ON_IDLE,
				   (GtkTreeIterCompareFunc) tree_model_compare_booleans,
				   GINT_TO_POINTER (MANAGER_COLUMN_LOCK_ON_IDLE), NULL);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (manager->priv->keyrings),
  				   MANAGER_COLUMN_LOCK_TIMEOUT,
				   (GtkTreeIterCompareFunc) tree_model_compare_uints,
				   GINT_TO_POINTER (MANAGER_COLUMN_LOCK_TIMEOUT), NULL);
  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (manager->priv->keyrings),
  				   MANAGER_COLUMN_MTIME,
				   (GtkTreeIterCompareFunc) tree_model_compare_uints,
				   GINT_TO_POINTER (MANAGER_COLUMN_MTIME), NULL);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (manager->priv->keyrings),
  				   MANAGER_COLUMN_CTIME,
				   (GtkTreeIterCompareFunc) tree_model_compare_uints,
				   GINT_TO_POINTER (MANAGER_COLUMN_CTIME), NULL);

  gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (manager->priv->keyrings),
				           (GtkTreeIterCompareFunc) tree_model_compare_strings,
				           GINT_TO_POINTER (MANAGER_COLUMN_KEYRING), NULL);
}

GtkTreeModel *
gkm_keyring_manager_get_model (GKMKeyringManager *self)
{
  g_return_val_if_fail (GKM_IS_KEYRING_MANAGER (self), NULL);

  return GTK_TREE_MODEL (self->priv->keyrings);
}

gchar *
gkm_keyring_manager_get_default_keyring (GKMKeyringManager *self)
{
  g_return_val_if_fail (GKM_IS_KEYRING_MANAGER (self), NULL);

  return g_strdup (self->priv->default_keyring);
}

/********************************************************************
 * Creating a keyring
 */

typedef struct _CreateKeyringCallbackData
{
  GKMKeyringManager *manager;
  gchar *keyring_name;
} CreateKeyringCallbackData;

static void
create_keyring_callback_data_free (CreateKeyringCallbackData *data)
{
  g_free (data->keyring_name);
  g_free (data);
}

static void
create_keyring_callback (GnomeKeyringResult         result, 
                         CreateKeyringCallbackData *data)
{
  GtkTreeIter iter;

  if (result != GNOME_KEYRING_RESULT_OK)
    {
      complain_about_gnome_keyring_bad_result (NULL, result);

      return;
    }

  gtk_list_store_append (data->manager->priv->keyrings, &iter);
  gtk_list_store_set (data->manager->priv->keyrings, &iter, 
		      MANAGER_COLUMN_KEYRING, data->keyring_name, -1);

  gkm_keyring_manager_update_keyring_info (data->manager, data->keyring_name, &iter);
}

void
gkm_keyring_manager_create_keyring (GKMKeyringManager *self,
				    const gchar       *name,
				    const gchar       *password)
{
  CreateKeyringCallbackData *data;
  
  data = g_new0 (CreateKeyringCallbackData, 1);
  data->manager = self;
  data->keyring_name = g_strdup (name);

  gnome_keyring_create (name, password,
			(GnomeKeyringOperationDoneCallback) create_keyring_callback,
			data, (GDestroyNotify) create_keyring_callback_data_free);
}

/********************************************************************
 * Deleting keyrings
 */

typedef struct _DeleteKeyringCallbackData
{
  GKMKeyringManager *manager;
  gchar             *keyring;
  GtkTreeIter	     iter;
} DeleteKeyringCallbackData;

static void
delete_keyring_done_callback (GnomeKeyringResult         result,
			      DeleteKeyringCallbackData *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      complain_about_gnome_keyring_bad_result (NULL, result);
      return;
    }

  gtk_list_store_remove (data->manager->priv->keyrings, &data->iter);
}

void
gkm_keyring_manager_delete_keyring (GKMKeyringManager *self,
				    GtkTreeIter        iter)
{
  DeleteKeyringCallbackData *data;

  g_return_if_fail (GKM_IS_KEYRING_MANAGER (self));

  data = g_new0 (DeleteKeyringCallbackData, 1);
  data->manager = self;
  data->iter = iter;

  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyrings), &data->iter, 
		      MANAGER_COLUMN_KEYRING, &data->keyring, -1);

  gnome_keyring_delete (data->keyring,
			(GnomeKeyringOperationDoneCallback) delete_keyring_done_callback,
			data,
			(GDestroyNotify) g_free);
}

/********************************************************************
 * Locking and unlocking keyrings
 */

/* typedef struct _LockUnlockKeyringCallbackData */
/* { */
/*   GtkTreeRowReference *row; */
/*   GKMKeyringManager *manager; */
/*   gchar *keyring_name; */
/* } LockUnlockKeyringCallbackData; */

/* static void */
/* lock_unlock_keyring_callback_data_free (LockUnlockKeyringCallbackData *data) */
/* { */
/*   gtk_tree_row_reference_free (data->row); */
/*   g_free (data->keyring_name); */
/*   g_free (data); */
/* } */

/* static LockUnlockKeyringCallbackData * */
/* lock_unlock_keyring_callback_data_clone (LockUnlockKeyringCallbackData *data) */
/* { */
/*   LockUnlockKeyringCallbackData *clone; */

/*   clone = g_new (LockUnlockKeyringCallbackData, 1); */
/*   clone->row = gtk_tree_row_reference_copy (data->row); */
/*   clone->manager = data->manager; */
/*   clone->keyring_name = g_strdup (data->keyring_name); */

/*   return clone; */
/* } */

/* static void  */
/* lock_keyring_callback (GnomeKeyringResult             result, */
/*                        LockUnlockKeyringCallbackData *data) */
/* { */
/*   GtkTreePath *path; */
/*   GtkTreeIter iter; */

/*   if (result != GNOME_KEYRING_RESULT_OK) */
/*     { */
/*       complain_about_gnome_keyring_bad_result (GTK_WINDOW (data->manager), result); */

/*       return; */
/*     }  */

/*   if (!gtk_tree_row_reference_valid (data->row)) */
/*     { */
/*       g_warning ("A row disappeared while we were waiting for it to be locked..."); */

/*       return; */
/*     } */

/*   path = gtk_tree_row_reference_get_path (data->row); */
/*   gtk_tree_model_get_iter (GTK_TREE_MODEL (data->manager->priv->keyrings), &iter, path); */
/*   gtk_tree_path_free (path); */

/*   gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_LOCK, */
/* 		      LOCKED_ICON, -1); */
/* }  */

/* static void  */
/* unlock_keyring_callback (GnomeKeyringResult             result, */
/*                          LockUnlockKeyringCallbackData *data) */
/* { */
/*   GtkTreePath *path; */
/*   GtkTreeIter iter; */

/*   if (result != GNOME_KEYRING_RESULT_OK) */
/*     { */
/*       complain_about_gnome_keyring_bad_result (GTK_WINDOW (data->manager),  */
/* 					       result); */

/*       return; */
/*     }  */

/*   if (!gtk_tree_row_reference_valid (data->row)) */
/*     { */
/*       return; */
/*     } */

/*   path = gtk_tree_row_reference_get_path (data->row); */
/*   if (!gtk_tree_model_get_iter (GTK_TREE_MODEL (data->manager->priv->keyrings),  */
/* 				&iter, path)) */
/*     { */
/*       g_assert_not_reached (); */
/*     } */
/*   gtk_tree_path_free (path); */

/*   gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_LOCK, */
/* 		      UNLOCKED_ICON, -1); */
/* } */

/* static void */
/* unlock_keyring_password_callback (GKMPasswordDialog             *dialog, */
/*                                   gint                           response, */
/*                                   LockUnlockKeyringCallbackData *data) */
/* { */
/*   g_return_if_fail (GKM_IS_PASSWORD_DIALOG (dialog)); */

/*   if (response == GTK_RESPONSE_ACCEPT) */
/*     { */
/*       char *password; */

/*       password = gkm_password_dialog_get_password (dialog); */
/*       gnome_keyring_unlock (data->keyring_name, password, */
/*                             (GnomeKeyringOperationDoneCallback) unlock_keyring_callback,  */
/*                             lock_unlock_keyring_callback_data_clone (data),  */
/*                             (GDestroyNotify) lock_unlock_keyring_callback_data_free); */
/*       g_free (password); */
/*     } */

/*   gtk_widget_destroy (GTK_WIDGET (dialog)); */
/* } */

/* static void */
/* lock_unlock_get_keyring_info_callback (GnomeKeyringResult             result,  */
/*                          	       GnomeKeyringInfo              *info, */
/* 				       LockUnlockKeyringCallbackData *data) */
/* { */
/*   if (result != GNOME_KEYRING_RESULT_OK)  */
/*     { */
/*       g_warning ("Failed to get keyring info."); */

/*       return; */
/*     } */

/*   if (gnome_keyring_info_get_is_locked (info)) */
/*     { */
/*       char *secondary_text; */

/*       if (data->manager->priv->unlock_dialog != NULL) */
/*         { */
/*           gtk_widget_destroy (data->manager->priv->unlock_dialog); */
/*         } */

/*       secondary_text = g_strdup_printf (_("A password is required in order to unlock the keyring '%s'."), data->keyring_name); */
/*       data->manager->priv->unlock_dialog = gkm_password_dialog_new (NULL, GTK_WINDOW (data->manager),  */
/*                                         GTK_DIALOG_DESTROY_WITH_PARENT, */
/*                                         _("Password required"), */
/* 					secondary_text); */
/*       g_free (secondary_text); */
/*       gkm_password_dialog_accept_button_set_label (GKM_PASSWORD_DIALOG (data->manager->priv->unlock_dialog), _("_Unlock")); */


/*       g_signal_connect_data (G_OBJECT (data->manager->priv->unlock_dialog), "response",  */
/*                              G_CALLBACK (unlock_keyring_password_callback), */
/*                              lock_unlock_keyring_callback_data_clone (data),  */
/*                              (GClosureNotify) lock_unlock_keyring_callback_data_free, */
/*                              0); */
/*       g_object_add_weak_pointer (G_OBJECT (data->manager->priv->unlock_dialog), (void**) &data->manager->priv->unlock_dialog); */

/*       gtk_window_present (GTK_WINDOW (data->manager->priv->unlock_dialog)); */
/*     } */
/*   else */
/*     { */
/*       gnome_keyring_lock (data->keyring_name, */
/*                           (GnomeKeyringOperationDoneCallback) lock_keyring_callback,  */
/*                           lock_unlock_keyring_callback_data_clone (data), (GDestroyNotify) lock_unlock_keyring_callback_data_free);  */
/*     } */
/* } */

/* static void */
/* lock_unlock_keyring_action_callback (GtkWidget         *widget G_GNUC_UNUSED, */
/* 			             GKMKeyringManager *manager) */
/* { */
/*   GtkTreeSelection *selection; */
/*   GtkTreeModel *model; */
/*   GtkTreeIter iter; */
/*   GtkTreeView *tree_view; */

/*   g_return_if_fail (GKM_IS_KEYRING_MANAGER (manager)); */

/*   tree_view = g_object_get_data (G_OBJECT (manager), "tree-view"); */

/*   selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view)); */
        
/*   if (gtk_tree_selection_get_selected (selection, &model, &iter)) */
/*     { */
/*       LockUnlockKeyringCallbackData *data; */
/*       GtkTreePath *path; */
/*       gchar *keyring_name; */

/*       gtk_tree_model_get (model, &iter, MANAGER_COLUMN_KEYRING, &keyring_name, -1); */
      
/*       data = g_new (LockUnlockKeyringCallbackData, 1); */

/*       path = gtk_tree_model_get_path (model, &iter); */
/*       data->row = gtk_tree_row_reference_new (model, path); */
/*       gtk_tree_path_free (path); */

/*       data->keyring_name = keyring_name; */
/*       data->manager = manager; */

/*       gnome_keyring_get_info (keyring_name,  */
/*                               (GnomeKeyringOperationGetKeyringInfoCallback) lock_unlock_get_keyring_info_callback,  */
/*                               data, (GDestroyNotify) lock_unlock_keyring_callback_data_free); */
/*     } */
/* } */

/********************************************************************
 * Opening keyrings
 */

/* static void */
/* open_keyring_password_callback (GKMPasswordDialog             *dialog, */
/*                                 gint                           response, */
/*                                 LockUnlockKeyringCallbackData *data) */
/* { */
/*   g_return_if_fail (GKM_IS_PASSWORD_DIALOG (dialog)); */

/*   if (response == GTK_RESPONSE_ACCEPT) */
/*     { */
/*       char *password; */

/*       password = gkm_password_dialog_get_password (dialog); */
/*       gnome_keyring_unlock (data->keyring_name, password, */
/*                             (GnomeKeyringOperationDoneCallback) open_keyring_callback,  */
/*                             lock_unlock_keyring_callback_data_clone (data),  */
/*                             (GDestroyNotify) lock_unlock_keyring_callback_data_free); */
/*       g_free (password); */
/*     } */

/*   gtk_widget_destroy (GTK_WIDGET (dialog)); */
/* } */

/* static void  */
/* open_get_keyring_info_callback (GnomeKeyringResult             result,  */
/*                          	GnomeKeyringInfo              *info, */
/* 				LockUnlockKeyringCallbackData *data) */
/* { */
/*  if (result != GNOME_KEYRING_RESULT_OK)  */
/*     { */
/*       g_warning ("Failed to get keyring info."); */

/*       return; */
/*     } */

/*   if (gnome_keyring_info_get_is_locked (info)) */
/*     { */
/*       gchar *primary_text; */

/*       if (data->manager->priv->unlock_dialog != NULL) */
/*         { */
/*           gtk_widget_destroy (data->manager->priv->unlock_dialog); */
/*         } */

/*       primary_text = g_strdup_printf (_("Unlock the “%s” Keyring."), data->keyring_name); */
/*       data->manager->priv->unlock_dialog = gkm_password_dialog_new (NULL, GTK_WINDOW (data->manager),  */
/*                                         GTK_DIALOG_DESTROY_WITH_PARENT, */
/*                                         primary_text, */
/*                                         _("To view the contents of this keyring, you must first unlock it.")); */
/*       g_free (primary_text); */

/*       g_signal_connect_data (G_OBJECT (data->manager->priv->unlock_dialog), "response",  */
/*                              G_CALLBACK (open_keyring_password_callback), */
/*                              lock_unlock_keyring_callback_data_clone (data),  */
/*                              (GClosureNotify) lock_unlock_keyring_callback_data_free, */
/*                              0); */
/*       g_object_add_weak_pointer (G_OBJECT (data->manager->priv->unlock_dialog), (void**) &data->manager->priv->unlock_dialog); */

/*       gtk_window_present (GTK_WINDOW (data->manager->priv->unlock_dialog)); */
/*     } */
/*   else */
/*     { */
/*       gkm_application_open_keyring_editor_for (data->keyring_name, NULL); */
/*     } */
/* } */

/* static void */
/* open_keyring_action_callback (GtkWidget         *button G_GNUC_UNUSED, */
/* 			      GKMKeyringManager *manager) */
/* { */
/*   GtkTreeSelection *selection; */
/*   GtkTreeModel *model; */
/*   GtkTreeIter iter; */

/*   g_return_if_fail (GKM_IS_KEYRING_MANAGER (manager)); */

/*   selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (manager->priv->tree_view)); */
  
/*   if (gtk_tree_selection_get_selected (selection, &model, &iter)) */
/*     { */
/*       LockUnlockKeyringCallbackData *data; */
/*       GtkTreePath *path; */
/*       gchar *keyring_name; */

/*       gtk_tree_model_get (model, &iter, MANAGER_COLUMN_KEYRING, &keyring_name, -1); */
      
/*       data = g_new0 (LockUnlockKeyringCallbackData, 1); */

/*       path = gtk_tree_model_get_path (model, &iter); */
/*       data->row = gtk_tree_row_reference_new (model, path); */
/*       gtk_tree_path_free (path); */

/*       data->keyring_name = keyring_name; */
/*       data->manager = manager; */

/*       gnome_keyring_get_info (keyring_name,  */
/*                               (GnomeKeyringOperationGetKeyringInfoCallback) open_get_keyring_info_callback,  */
/*                               data, (GDestroyNotify) lock_unlock_keyring_callback_data_free); */
/*     } */

/*   return; */
/* } */

/********************************************************************
 * Set the default keyring
 */

/* static void */
/* set_default_keyring_worker_callback (GnomeKeyringResult  result, */
/*                                      GKMKeyringManager  *manager) */
/* { */
/*   if (result != GNOME_KEYRING_RESULT_OK) */
/*     { */
/*       /\* FIXME: tell the user about this. *\/ */
/*       g_warning ("Failed set the default keyring."); */

/*       return; */
/*     } */

/*   gkm_keyring_manager_update_default_keyring (manager); */
/* } */

/* static void */
/* set_default_keyring_action_callback (GtkWidget         *widget G_GNUC_UNUSED, */
/*                                      GKMKeyringManager *manager) */
/* { */
/*   GtkTreeView *tree_view; */
/*   GtkTreeSelection *selection; */
/*   GtkTreeModel *model; */
/*   GtkTreeIter iter; */

/*   g_return_if_fail (GTK_IS_WINDOW (manager)); */

/*   tree_view = g_object_get_data (G_OBJECT (manager), "tree-view"); */

/*   selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view)); */

/*   if (gtk_tree_selection_get_selected (selection, &model, &iter)) */
/*     { */
/*       char *keyring; */

/*       gtk_tree_model_get (GTK_TREE_MODEL (manager->priv->keyrings), &iter,  */
/* 			  MANAGER_COLUMN_KEYRING, &keyring, -1); */

/*       gnome_keyring_set_default_keyring (keyring,  */
/*                                          (GnomeKeyringOperationDoneCallback) set_default_keyring_worker_callback, */
/*                                          manager, NULL); */

/*       g_free (keyring); */
/*     } */
/* } */

/********************************************************************
 * Searching for a keyring
 */

typedef struct _SearchKeyringCallbackData
{
  const char  *keyring;
  GtkTreeIter  iter;
  gboolean     found;
} SearchKeyringCallbackData;

static gboolean
search_keyring_worker (GtkTreeModel              *model,
                       GtkTreePath               *path G_GNUC_UNUSED,
                       GtkTreeIter               *iter,
                       SearchKeyringCallbackData *data)
{
  char *keyring;
  
  g_assert (data != NULL);

  gtk_tree_model_get (model, iter, MANAGER_COLUMN_KEYRING, &keyring, -1);

  if (strcmp (keyring, data->keyring) == 0) 
    {
      data->iter = *iter;
      data->found = TRUE;
    }

  g_free (keyring);

  return data->found;
}

/**
 * gkm_keyring_manager_search_keyring:
 * @manager: a #GKMKeyringManager.
 * @keyring: name of the keyring to look for.
 * @iter: a pointer to a #GtkTreeIter or %NULL.
 * @path: a pointer to a pointer to a #GtkTreePath or %NULL.
 *
 * Looks for @keyring in the keyrings #GtkTreeModel, and if it find it,
 * fills those of @iter, @path, and @row which are not %NULL so that 
 * they point to it.
 *
 * Returns: %TRUE if @keyring was found; %FALSE otherwise.
 **/

static gboolean
gkm_keyring_manager_search_keyring (GKMKeyringManager    *manager,
                                    const char           *keyring, 
                                    GtkTreeIter          *iter, 
                                    GtkTreePath         **path,
                                    GtkTreeRowReference **row)
{
  SearchKeyringCallbackData data;

  data.keyring = keyring;
  data.found = FALSE;

  gtk_tree_model_foreach (GTK_TREE_MODEL (manager->priv->keyrings), (GtkTreeModelForeachFunc) search_keyring_worker, &data);

  if (data.found)
    {
      if (iter != NULL)
        {
          *iter = data.iter;
        }
      if (path != NULL)
        {
          *path = gtk_tree_model_get_path (GTK_TREE_MODEL (manager->priv->keyrings), &data.iter);
        }
      if (row != NULL)
        {
          if (path != NULL)
            {
              *row = gtk_tree_row_reference_new (GTK_TREE_MODEL (manager->priv->keyrings), *path);
            }
          else
            {
              GtkTreePath *tmp_path;

              tmp_path = gtk_tree_model_get_path (GTK_TREE_MODEL (manager->priv->keyrings), &data.iter);

              *row = gtk_tree_row_reference_new (GTK_TREE_MODEL (manager->priv->keyrings), tmp_path);
              gtk_tree_path_free (tmp_path);
            }
        }
    }

  return data.found;
}

/********************************************************************
 * Getting the default keyring
 */

static void
get_default_keyring_callback (GnomeKeyringResult  result,
                              const char         *new_default_keyring,
                              GKMKeyringManager  *manager)
{
  GtkTreeIter iter;

  if (result != GNOME_KEYRING_RESULT_OK)
    {
      g_warning ("Failed get the default keyring.");

      return;
    }

  if (manager->priv->default_keyring != NULL)
    {
      if (new_default_keyring != NULL && g_str_equal (manager->priv->default_keyring, new_default_keyring))
        {
          return;
        }
      else if (gkm_keyring_manager_search_keyring (manager, manager->priv->default_keyring, &iter, NULL, NULL))
        {
          gtk_list_store_set (manager->priv->keyrings, &iter, MANAGER_COLUMN_DEFAULT, FALSE, -1);
        }
      else
        {
          g_warning ("The default keyring is not in the list store!");
        }

      g_free (manager->priv->default_keyring);
    }

  manager->priv->default_keyring = g_strdup (new_default_keyring);

  if (manager->priv->default_keyring != NULL 
      && gkm_keyring_manager_search_keyring (manager, 
					     manager->priv->default_keyring, 
					     &iter, NULL, NULL))
    {
      gtk_list_store_set (manager->priv->keyrings, &iter, 
			  MANAGER_COLUMN_DEFAULT, TRUE, -1);
    }
}

static void
gkm_keyring_manager_update_default_keyring (GKMKeyringManager *manager)
{
  gnome_keyring_get_default_keyring ((GnomeKeyringOperationGetStringCallback) get_default_keyring_callback, 
                                     manager, NULL);
}


/********************************************************************
 * Updating the info on a keyring
 */

typedef struct _UpdateKeyringCallbackData
{
  GKMKeyringManager *manager;
  GtkTreeRowReference *row;
} UpdateKeyringCallbackData;

static void
update_keyring_callback_data_free (UpdateKeyringCallbackData *data)
{
  gtk_tree_row_reference_free (data->row);
  g_free (data);
}

static void
update_keyring_info_worker_callback (GnomeKeyringResult         result,
                                     GnomeKeyringInfo          *info,
                                     UpdateKeyringCallbackData *data)
{
  gboolean value;
  const char *stock_id;
  time_t time;
  guint32 seconds;
  GtkTreePath *path;
  GtkTreeIter iter;
  
  if (result != GNOME_KEYRING_RESULT_OK) 
    {
      g_warning ("Failed to get keyring info.");

      return;
    }

  if (!gtk_tree_row_reference_valid (data->row))
    {
      g_warning ("A row disappeared while we were waiting for the data...");

      return;
    }

  path = gtk_tree_row_reference_get_path (data->row);
  gtk_tree_model_get_iter (GTK_TREE_MODEL (data->manager->priv->keyrings), &iter, path);
  gtk_tree_path_free (path);
  
  stock_id = (gnome_keyring_info_get_is_locked (info) == TRUE) ? LOCKED_ICON : UNLOCKED_ICON;
  gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_LOCK, stock_id, -1);
  
  value = gnome_keyring_info_get_lock_on_idle (info);
  gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_LOCK_ON_IDLE, value, -1);
  
  seconds =  gnome_keyring_info_get_lock_timeout (info);
  gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_LOCK_TIMEOUT, seconds, -1);
  
  time = gnome_keyring_info_get_ctime (info);
  gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_CTIME, time, -1);

  time = gnome_keyring_info_get_mtime (info);
  gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_MTIME, time, -1);

  gtk_list_store_set (data->manager->priv->keyrings, &iter, MANAGER_COLUMN_DEFAULT, FALSE, -1);
}

/**
 * gkm_keyring_manager_update_keyring_info:
 * @manager: a #GKMKeyringManager.
 * @keyring: the name of the keyring whose info is to be updated.
 * @iter: a #GtkTreeIter pointing to @keyring in the keyrings @GtkListStore, 
 * or %NULL.
 *
 * Update the information on @keyring in the keyrings liststore.
 **/

static void
gkm_keyring_manager_update_keyring_info (GKMKeyringManager *manager, 
                                         const char        *keyring, 
                                         GtkTreeIter       *iter)
{
  UpdateKeyringCallbackData *data;

  data = g_new0 (UpdateKeyringCallbackData, 1);
  data->manager = manager;

  if (iter == NULL)
    {
      if (!gkm_keyring_manager_search_keyring (manager, keyring, NULL, NULL, &data->row))
        {
          g_warning ("gkm_keyring_manager_update_keyring_info: Tried to update the information of a keyring we don't know about");
        }
    }
  else
    {
      GtkTreePath *path;

      path = gtk_tree_model_get_path (GTK_TREE_MODEL (manager->priv->keyrings), iter);
      data->row = gtk_tree_row_reference_new (GTK_TREE_MODEL (manager->priv->keyrings), path);
      gtk_tree_path_free (path);
    }

  gnome_keyring_get_info (keyring, 
                          (GnomeKeyringOperationGetKeyringInfoCallback) update_keyring_info_worker_callback, 
                          data, (GDestroyNotify) update_keyring_callback_data_free);
}

/********************************************************************
 * Getting the list of all keyrings
 */

static void
update_keyrings_worker_callback (GnomeKeyringResult  result, 
                                 GList              *list, 
                                 GKMKeyringManager  *manager)
{
  GList *tmp;

  g_return_if_fail (GKM_IS_KEYRING_MANAGER (manager));

  if (result != GNOME_KEYRING_RESULT_OK)
    {
      /* FIXME: We pass here NULL because this will be called while the manager is initializing, so
       * the window will not be shown yet
       */ 
      complain_about_gnome_keyring_bad_result (NULL, result);

      return;
    }
       
  gtk_list_store_clear (manager->priv->keyrings);

  for (tmp = list; tmp != NULL; tmp = tmp->next)
    {
      GtkTreeIter iter;

      gtk_list_store_append (manager->priv->keyrings, &iter);
      gtk_list_store_set (manager->priv->keyrings, &iter, MANAGER_COLUMN_KEYRING, tmp->data, -1);

      gkm_keyring_manager_update_keyring_info (manager, tmp->data, &iter);
    }

  gkm_keyring_manager_update_default_keyring (manager);
}

static void
gkm_keyring_manager_update_keyrings (GKMKeyringManager *self)
{
  gnome_keyring_list_keyring_names ((GnomeKeyringOperationGetListCallback) 
				    update_keyrings_worker_callback, 
                                    self, NULL);
}
