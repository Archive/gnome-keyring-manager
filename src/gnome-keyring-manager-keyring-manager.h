/*
   Copyright (C) 2004 Fernando Herrera <fherrera@onirica.com>
   Copyright (C) 2004 Mariano Suárez-Alvarez <mariano@gnome.org>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GNOME_KEYRING_MANAGER_KEYRING_MANAGER_H
#define GNOME_KEYRING_MANAGER_KEYRING_MANAGER_H

#include <config.h>

#include <glib-object.h>

G_BEGIN_DECLS

enum
{
  MANAGER_COLUMN_KEYRING = 0,
  MANAGER_COLUMN_LOCK,
  MANAGER_COLUMN_LOCK_ON_IDLE,
  MANAGER_COLUMN_LOCK_TIMEOUT,
  MANAGER_COLUMN_MTIME,
  MANAGER_COLUMN_CTIME,
  MANAGER_COLUMN_DEFAULT,
  NUM_MANAGER_COLUMNS
} GKMKeyringManagerColumns;

#define GKM_TYPE_KEYRING_MANAGER            (gkm_keyring_manager_get_type ())
#define GKM_KEYRING_MANAGER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GKM_TYPE_KEYRING_MANAGER, GKMKeyringManager))
#define GKM_KEYRING_MANAGER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GKM_TYPE_KEYRING_MANAGER, GKMKeyringManagerClass))
#define GKM_IS_KEYRING_MANAGER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GKM_TYPE_KEYRING_MANAGER))
#define GKM_IS_KEYRING_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GKM_TYPE_KEYRING_MANAGER))
#define GKM_KEYRING_MANAGER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GKM_TYPE_KEYRING_MANAGER, GKMKeyringManagerClass))

typedef struct _GKMKeyringManager         GKMKeyringManager;
typedef struct _GKMKeyringManagerClass    GKMKeyringManagerClass;
typedef struct _GKMKeyringManagerPrivate  GKMKeyringManagerPrivate;

struct _GKMKeyringManager
{
  GObject parent;

  GKMKeyringManagerPrivate *priv;
};

struct _GKMKeyringManagerClass
{
  GObjectClass parent;
};

G_GNUC_CONST GType gkm_keyring_manager_get_type (void);

GObject *gkm_keyring_manager_new (void);

GtkTreeModel *gkm_keyring_manager_get_model           (GKMKeyringManager *self);
gchar        *gkm_keyring_manager_get_default_keyring (GKMKeyringManager *self);

void          gkm_keyring_manager_create_keyring      (GKMKeyringManager *self,
					               const gchar       *name,
					               const gchar       *password);

void          gkm_keyring_manager_delete_keyring      (GKMKeyringManager *self,
						       GtkTreeIter        iter);

G_END_DECLS

#endif
