/* 
   Copyright (C) 2004 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gconf/gconf-client.h>
#include <gnome-keyring.h>

#include "gnome-keyring-manager.h"
#include "gnome-keyring-manager-util.h"
#include "gnome-keyring-manager-attribute-display.h"
#include "gnome-keyring-manager-keyring-manager.h"
#include "gnome-keyring-manager-keyring-editor.h"

#define GKM_KEYRING_EDITOR_GCONF_DIR GNOME_KEYRING_MANAGER_GCONF_PREFIX "/keyring-editor"

struct _GKMKeyringEditorPrivate
{
  gboolean is_multiline_secret;

  GtkListStore *keyring_items;

  char *keyring_name;

  GSList *gconf_cnxn_ids;
};

static void gkm_keyring_editor_finalize   (GObject *object);

const char *keyring_item_type_text_from_enum (GnomeKeyringItemType type);
static void gkm_keyring_editor_setup_keyring_items_tree (GKMKeyringEditor *editor);

/* static void add_keyring_item_callback (GtkWidget *widget,  */
/* 				       GKMKeyringEditor *editor); */

static void gkm_keyring_editor_update_keyring_items (GKMKeyringEditor *self);
static void gkm_keyring_editor_update_keyring_item_info (GKMKeyringEditor *self, 
							 guint32 item_id, 
							 GtkTreeIter *iter);

G_DEFINE_TYPE (GKMKeyringEditor, gkm_keyring_editor, G_TYPE_OBJECT);

static void
gkm_keyring_editor_class_init (GKMKeyringEditorClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  gobject_class->finalize = gkm_keyring_editor_finalize;
}

static void
gkm_keyring_editor_init (GKMKeyringEditor *editor)
{
  editor->priv = g_new0 (GKMKeyringEditorPrivate, 1);

  gkm_keyring_editor_setup_keyring_items_tree (editor);
}

static void
gkm_keyring_editor_finalize (GObject *object)
{
  GKMKeyringEditor *editor;

  g_return_if_fail (GKM_IS_KEYRING_EDITOR (object));

  editor = GKM_KEYRING_EDITOR (object);

  g_free (editor->priv->keyring_name);
 
  g_free (editor->priv);

  G_OBJECT_CLASS (gkm_keyring_editor_parent_class)->finalize (object);
}

GObject *
gkm_keyring_editor_new (const char *keyring_name)
{
  GKMKeyringEditor *editor;

  editor = g_object_new (GKM_TYPE_KEYRING_EDITOR, NULL);

  if (keyring_name)
    gkm_keyring_editor_set_keyring_name (editor, keyring_name);
  
  return G_OBJECT (editor);
}

static gint
compare_keyring_item_types (GtkTreeModel *model,
			    GtkTreeIter  *a,
			    GtkTreeIter  *b,
			    gpointer      data)
{
  gint column;
  int type1;
  int type2;

  column = GPOINTER_TO_INT (data);

  gtk_tree_model_get (model, a, column, &type1, -1);
  gtk_tree_model_get (model, b, column, &type2, -1);

  return g_utf8_collate (keyring_item_type_text_from_enum (type1), keyring_item_type_text_from_enum (type2));

}

const char *
keyring_item_type_text_from_enum (GnomeKeyringItemType type)
{
  const char *text;
  
  switch (type)
    {
      case GNOME_KEYRING_ITEM_GENERIC_SECRET:
        text = _("Secret");
        break;
      case GNOME_KEYRING_ITEM_NETWORK_PASSWORD:
        text = _("Network Password");
        break;
      case GNOME_KEYRING_ITEM_NOTE:
        text = _("Note");
        break;
      default:
        g_warning ("Unknown keyring item type: %d", type);
        text = "";
        break;
    }

  return text;
}

static void
gkm_keyring_editor_setup_keyring_items_tree (GKMKeyringEditor *editor)
{
  g_return_if_fail (GKM_IS_KEYRING_EDITOR (editor));
  g_assert (editor->priv->keyring_items == NULL);
  
  editor->priv->keyring_items = gtk_list_store_new (NUM_EDITOR_COLUMNS, 
                                                    /* EDITOR_COLUMN_NAME */   G_TYPE_STRING, 
                                                    /* EDITOR_COLUMN_TYPE */   G_TYPE_INT, 
                                                    /* EDITOR_COLUMN_MTIME */  G_TYPE_UINT, 
                                                    /* EDITOR_COLUMN_CTIME */  G_TYPE_UINT, 
                                                    /* EDITOR_COLUMN_ID */     G_TYPE_UINT, 
                                                    /* EDITOR_COLUMN_SECRET */ G_TYPE_STRING);
  
  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (editor->priv->keyring_items), EDITOR_COLUMN_TYPE, 
                                   (GtkTreeIterCompareFunc) compare_keyring_item_types, GINT_TO_POINTER (EDITOR_COLUMN_TYPE), NULL);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (editor->priv->keyring_items),
  				   EDITOR_COLUMN_NAME,
				   (GtkTreeIterCompareFunc) tree_model_compare_strings,
				   GINT_TO_POINTER (EDITOR_COLUMN_NAME), NULL);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (editor->priv->keyring_items),
  				   EDITOR_COLUMN_MTIME,
				   (GtkTreeIterCompareFunc) tree_model_compare_uints,
				   GINT_TO_POINTER (EDITOR_COLUMN_MTIME), NULL);

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (editor->priv->keyring_items),
  				   EDITOR_COLUMN_CTIME,
				   (GtkTreeIterCompareFunc) tree_model_compare_uints,
				   GINT_TO_POINTER (EDITOR_COLUMN_CTIME), NULL);

  gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (editor->priv->keyring_items),
				           (GtkTreeIterCompareFunc) tree_model_compare_uints,
				           GINT_TO_POINTER (EDITOR_COLUMN_ID), NULL);
}

/* TODO: these functions should be used for setting proper gobject properties as well*/
GtkTreeModel *
gkm_keyring_editor_get_model (GKMKeyringEditor *self)
{
  g_return_val_if_fail (GKM_IS_KEYRING_EDITOR (self), NULL);

  return GTK_TREE_MODEL (self->priv->keyring_items);
}

void 
gkm_keyring_editor_set_keyring_name (GKMKeyringEditor *self, 
				     const gchar      *keyring_name)
{
  g_return_if_fail (GKM_IS_KEYRING_EDITOR (self));

  if (keyring_name != NULL)
    {
      self->priv->keyring_name = g_strdup (keyring_name);
    }
  
  gkm_keyring_editor_update_keyring_items (self);
}

gchar *
gkm_keyring_editor_get_keyring_name (GKMKeyringEditor *self)
{
  g_return_val_if_fail (GKM_IS_KEYRING_EDITOR (self), NULL);

  return g_strdup (self->priv->keyring_name);
}

/***************************************************************************
 * Create a new keyring item.
 */

/* static void */
/* add_keyring_item_done_callback (GnomeKeyringResult result, */
/* 				guint32 val G_GNUC_UNUSED, */
/* 				GKMKeyringEditor *editor) */
/* { */
/*   if (result != GNOME_KEYRING_RESULT_OK) */
/*     { */
/*       //TODO: This should be a signal */
/*       complain_about_gnome_keyring_bad_result (NULL, result); */

/*       return; */
/*     }  */

/*   gkm_keyring_editor_update_keyring_items (editor);  */
/* } */

/* static void */
/* add_keyring_item_action_callback (GKMNewItemDialog *dialog, */
/* 				  gint              response_id, */
/* 				  GKMKeyringEditor *editor) */
/* { */
/*   g_return_if_fail (GKM_IS_NEW_ITEM_DIALOG (dialog)); */
/*   g_return_if_fail (GKM_IS_KEYRING_EDITOR (editor)); */

/*   switch (response_id) */
/*     { */
/*       case GTK_RESPONSE_CANCEL: */
/*         break; */
/*       case GTK_RESPONSE_ACCEPT: */
/*         { */
/* 	  GnomeKeyringItemType       type; */
/* 	  const gchar               *item_name; */
/* 	  gchar                     *secret; */
/* 	  GnomeKeyringAttributeList *list; */
	  
/*           type = gkm_new_item_dialog_get_item_type (dialog); */
/* 	  item_name = gkm_new_item_dialog_get_item_name (dialog); */
/* 	  secret = gkm_new_item_dialog_get_item_secret (dialog); */
/* 	  list = gkm_new_item_dialog_get_item_attribute_list (dialog); */
	  
/*           gnome_keyring_item_create (editor->priv->keyring_name, */
/* 				     type, */
/* 				     item_name, */
/* 				     list, */
/* 				     secret, */
/* 				     FALSE, */
/* 				     (GnomeKeyringOperationGetIntCallback) add_keyring_item_done_callback, */
/* 				     editor, NULL); */
        
/*           gnome_keyring_attribute_list_free (list); */
/* 	  g_free (secret); */

/*           break; */
/* 	} */
/*     } */

/*   gtk_widget_destroy (GTK_WIDGET (dialog)); */
/* } */

/* static void */
/* add_keyring_item_callback (GtkWidget        *button G_GNUC_UNUSED, */
/*  		           GKMKeyringEditor *editor) */
/* { */
/*   GtkWidget *dialog; */

/*   g_return_if_fail (GKM_IS_KEYRING_EDITOR (editor)); */

/*   dialog = gkm_new_item_dialog_new (editor->priv->keyring_name, GTK_WINDOW (editor)); */

/*   g_signal_connect (G_OBJECT (dialog),  */
/*   		    "response",  */
/* 		    G_CALLBACK (add_keyring_item_action_callback),  */
/* 		    editor); */

/*   gtk_window_present (GTK_WINDOW (dialog)); */
/* } */

/***********************************************
 * Setting key attributes.
 */

typedef struct _SaveItemCallbackData
{
  GKMKeyringEditor *editor;
  guint32	    item_id;
  GtkTreeIter       iter;
} SaveItemCallbackData;

static void
attributes_saved_callback (GnomeKeyringResult result,
			   SaveItemCallbackData *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      complain_about_gnome_keyring_bad_result (NULL, result);
      return;
    }

  gkm_keyring_editor_update_keyring_item_info (data->editor, data->item_id, &data->iter);

  /* TODO: This should be in main_ui */
/*   selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(data->editor->priv->items_tree_view)); */
/*   keyring_item_tree_selection_changed (selection, data->editor); */
}

void
gkm_keyring_editor_set_attributes_list (GKMKeyringEditor *self,
					GtkTreeIter       iter,
					GnomeKeyringAttributeList *attributes)
{
  SaveItemCallbackData *data;

  data = g_new0 (SaveItemCallbackData, 1);

  data->editor = self;
  data->iter = iter;

  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyring_items), &iter,
		      EDITOR_COLUMN_ID, &data->item_id, -1);

  gnome_keyring_item_set_attributes (data->editor->priv->keyring_name, 
				     data->item_id, 
				     attributes,
				     (GnomeKeyringOperationDoneCallback) 
				     attributes_saved_callback,
				     data, g_free);

  gnome_keyring_attribute_list_free (attributes);
}

static void
acl_saved_callback (GnomeKeyringResult result,
		    SaveItemCallbackData *data G_GNUC_UNUSED)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      complain_about_gnome_keyring_bad_result (NULL, result);
      return;
    }

/*   if (!gtk_tree_row_reference_valid (data->row)) */
/*     { */
/*       g_warning ("A row disappeared while we were waiting for the data..."); */

/*       return; */
/*     } */
}


void 
gkm_keyring_editor_set_acl (GKMKeyringEditor *self,
			    GtkTreeIter       iter,
			    GList            *acl)
{
  SaveItemCallbackData *data;

  data = g_new0 (SaveItemCallbackData, 1);

  data->editor = self;
  data->iter = iter;

  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyring_items), &iter, 
		      EDITOR_COLUMN_ID, &data->item_id, -1);

  gnome_keyring_item_set_acl (self->priv->keyring_name, data->item_id, acl,
			      (GnomeKeyringOperationDoneCallback) 
			      acl_saved_callback,
			      data, g_free);

  /* FIXME: This is leaking the acl here. Previous 
     gnome_keyring_acl_free (acl) made of course crash it.
     we should add the acl to the data and free it on the destroy
     callback, but that is also crashing. Need investigation */
}

/********************************************************
 * Change the name of a keyring item.
 */

typedef struct _RenameItemCallbackData
{
  GKMKeyringEditor *editor;
  GtkTreeIter	    iter;
  guint32           item_id;
  gchar            *new_name;
} RenameItemCallbackData;

static void
rename_item_done_callback (GnomeKeyringResult        result,
			   RenameItemCallbackData   *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      complain_about_gnome_keyring_bad_result (NULL, result);
      return;
    }

  gtk_list_store_set (data->editor->priv->keyring_items, &data->iter,
		      EDITOR_COLUMN_NAME, data->new_name, -1);
}

static void
rename_item_get_info_callback (GnomeKeyringResult      result,
			       GnomeKeyringItemInfo   *info,
			       RenameItemCallbackData *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      complain_about_gnome_keyring_bad_result (NULL, result);
      g_free (data);
      return;
    }
  
  gnome_keyring_item_info_set_display_name (info, data->new_name);
  
  gnome_keyring_item_set_info (data->editor->priv->keyring_name, 
			       data->item_id, info,
			       (GnomeKeyringOperationDoneCallback)
			       rename_item_done_callback,
			       data, g_free);
}

void 
gkm_keyring_editor_set_key_name (GKMKeyringEditor *self,
				 GtkTreeIter       iter,
				 const gchar      *new_name)
{
  RenameItemCallbackData *data;

  data = g_new0 (RenameItemCallbackData, 1);

  data->editor = self;
  data->iter = iter;
  data->new_name = g_strdup (new_name);

  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyring_items), &iter, 
		      EDITOR_COLUMN_ID, &data->item_id, -1);

  gnome_keyring_item_get_info (self->priv->keyring_name, data->item_id,
			       (GnomeKeyringOperationGetItemInfoCallback)
			       rename_item_get_info_callback,
			       data, NULL);
}


/********************************************************
 * Delete a keyring item
 */

typedef struct _DeleteItemCallbackData
{
  GKMKeyringEditor *editor;
  GtkTreeIter	    iter;
  guint32           item_id;
} DeleteItemCallbackData;

static void
delete_item_done_callback (GnomeKeyringResult        result,
			   DeleteItemCallbackData   *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      complain_about_gnome_keyring_bad_result (NULL, result);
      return;
    }

  gtk_list_store_remove (data->editor->priv->keyring_items, &data->iter);

  /* FIXME: This should be in the ui section (callback or signal) */

/*   if (gtk_list_store_remove (data->editor->priv->keyring_items, &data->iter)) */
/*     { */
/*       gtk_tree_selection_select_iter (data->selection, &data->iter); */
/*     } */
/*   else */
/*     { */
/*       if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (data->editor->priv->keyring_items), &data->iter)) */
/*         { */
/* 	  gtk_tree_selection_select_iter (data->selection, &data->iter); */
/*         } */
/*     } */
}

void
gkm_keyring_editor_delete_key (GKMKeyringEditor *self,
			       GtkTreeIter       iter)
{
  DeleteItemCallbackData *data;

  g_return_if_fail (GKM_IS_KEYRING_EDITOR (self));

  data = g_new0 (DeleteItemCallbackData, 1);
  data->editor = self;
  data->iter = iter;

  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyring_items), &data->iter, 
		      EDITOR_COLUMN_ID, &data->item_id, -1);

  gnome_keyring_item_delete (data->editor->priv->keyring_name,
			     data->item_id,
			     (GnomeKeyringOperationDoneCallback) delete_item_done_callback,
			     data,
			     (GDestroyNotify) g_free);
}
 
/********************************************************
 * Change the keyring secret
 */

typedef struct _ChangeSecretCallbackData
{
  GKMKeyringEditor *editor;
  char *old_secret;
  char *new_secret;
  guint32 item_id;
  GtkTreeIter iter;
} ChangeSecretCallbackData;

static ChangeSecretCallbackData *
change_secret_callback_data_clone (ChangeSecretCallbackData *data)
{
  ChangeSecretCallbackData *data_new;

  data_new = g_new0 (ChangeSecretCallbackData, 1);

  data_new->editor = data->editor;
  data_new->item_id = data->item_id;
  data_new->old_secret = g_strdup (data->old_secret);
  data_new->new_secret = g_strdup (data->new_secret);
  data_new->iter = data->iter;

  return data_new;
}

static void
change_secret_callback_data_free (ChangeSecretCallbackData *data)
{
  g_free (data->old_secret);
  g_free (data->new_secret);
  g_free (data);
}

static void
change_secret_set_info_callback (GnomeKeyringResult        result,
				 ChangeSecretCallbackData *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
/*       gtk_label_set_text (GTK_LABEL (data->editor->priv->secret_field), data->old_secret); */
/*       gkm_keyring_editor_set_secret (data->editor, data->old_secret); */
      
      complain_about_gnome_keyring_bad_result (GTK_WINDOW (data->editor), result);
      return;
    }

   gtk_list_store_set (data->editor->priv->keyring_items, &data->iter, 
		       EDITOR_COLUMN_SECRET, data->new_secret, -1);
}

static void
change_secret_get_info_callback (GnomeKeyringResult        result,
			         GnomeKeyringItemInfo     *info,
			         ChangeSecretCallbackData *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
/*       gkm_keyring_editor_set_secret (data->editor, data->old_secret); */
 
      complain_about_gnome_keyring_bad_result (GTK_WINDOW (data->editor), result);
      return;
    }

  gnome_keyring_item_info_set_secret (info, data->new_secret);

  gnome_keyring_item_set_info (data->editor->priv->keyring_name,
  			       data->item_id,
  			       info,
			       (GnomeKeyringOperationDoneCallback) change_secret_set_info_callback,
			       change_secret_callback_data_clone (data),
			       (GDestroyNotify) change_secret_callback_data_free);
}

void 
gkm_keyring_editor_set_key_secret      (GKMKeyringEditor *self,
					GtkTreeIter       iter,
					const gchar      *new_secret)
{
  guint32  item_id;
  gchar   *secret;

  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyring_items), &iter, 
		      EDITOR_COLUMN_ID, &item_id, 
		      EDITOR_COLUMN_SECRET, &secret, -1);

  if (!g_str_equal (new_secret, secret))
    {
      ChangeSecretCallbackData *data;
      
      data = g_new0 (ChangeSecretCallbackData, 1);
      data->editor = self;
      data->item_id = item_id;
      data->old_secret = g_strdup (secret);
      data->new_secret = g_strdup (new_secret);
      data->iter = iter;
      
      gnome_keyring_item_get_info (self->priv->keyring_name,
				   item_id,
				   (GnomeKeyringOperationGetItemInfoCallback) 
				   change_secret_get_info_callback,
				   data,
				   (GDestroyNotify) change_secret_callback_data_free);
    }
}

/********************************************************
 * Get the attributes of the selected key.
 */

typedef struct _GetAttributesCallbackData
{
  GKMKeyringEditor *editor;
  GnomeKeyringItemType item_type;
  GKMKeyringEditorGetAttributesCallback callback_func;
  gpointer  callback_data;
} GetAttributesCallbackData;

static void
gkm_keyring_editor_get_attributes_callback (GnomeKeyringResult         result,
					    GnomeKeyringAttributeList *attributes,
					    GetAttributesCallbackData *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      /* fixme should be passing gerrors back, instead */
      complain_about_gnome_keyring_bad_result (NULL, result);
      return;
    }

  (*data->callback_func) (data->callback_data, data->item_type, attributes);
}

void 
gkm_keyring_editor_get_attributes_list (GKMKeyringEditor *self,
					GtkTreeIter       iter,
					GKMKeyringEditorGetAttributesCallback callback,
					gpointer          user_data)
{
  GetAttributesCallbackData *data;
  guint32 item_id;

  g_return_if_fail (GKM_IS_KEYRING_EDITOR (self));

  data = g_new0 (GetAttributesCallbackData, 1);
  data->editor = self;
  data->callback_func = callback;
  data->callback_data = user_data;
  
  
  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyring_items), &iter, 
		      EDITOR_COLUMN_ID, &item_id, 
		      EDITOR_COLUMN_TYPE, &data->item_type, 
		      -1);
  
  gnome_keyring_item_get_attributes (self->priv->keyring_name,
				     item_id,
				     (GnomeKeyringOperationGetAttributesCallback) 
				     gkm_keyring_editor_get_attributes_callback,
				     data, g_free);
}

/********************************************************
 * Get the acl of the selected key.
 */

typedef struct _GetAclCallbackData
{
  GKMKeyringEditor *editor;
  GKMKeyringEditorGetAclCallback callback_func;
  gpointer  callback_data;
} GetAclCallbackData;

static void
gkm_keyring_editor_get_acl_callback (GnomeKeyringResult  result,
				     GSList             *acl,
				     GetAclCallbackData *data)
{
  if (result != GNOME_KEYRING_RESULT_OK)
    {
      /* fixme should be passing gerrors back, instead */
      complain_about_gnome_keyring_bad_result (NULL, result);
      return;
    }

  (*data->callback_func) (data->callback_data, acl);
}

void 
gkm_keyring_editor_get_acl (GKMKeyringEditor *self,
			    GtkTreeIter       iter,
			    GKMKeyringEditorGetAclCallback callback,
			    gpointer          user_data)
{
  GetAclCallbackData *data;
  guint32 item_id;

  g_return_if_fail (GKM_IS_KEYRING_EDITOR (self));

  data = g_new0 (GetAclCallbackData, 1);
  data->editor = self;
  data->callback_func = callback;
  data->callback_data = user_data;
  
  
  gtk_tree_model_get (GTK_TREE_MODEL (self->priv->keyring_items), &iter, 
		      EDITOR_COLUMN_ID, &item_id, 
		      -1);
  
  gnome_keyring_item_get_acl (self->priv->keyring_name,
			      item_id,
			      (GnomeKeyringOperationGetListCallback) 
			      gkm_keyring_editor_get_acl_callback,
			      data, g_free);
}

/* static void */
/* keyring_editor_update_sensitivity (GKMKeyringEditor *editor,  */
/*                                    gboolean sensitive) */
/* { */
/*   gtk_widget_set_sensitive (editor->priv->delete_button, sensitive); */
/*   gtk_widget_set_sensitive (editor->priv->edit_button, sensitive); */
/*   gtk_widget_set_sensitive (editor->priv->secret_label, sensitive); */
/*   gtk_widget_set_sensitive (editor->priv->secret_event_box, sensitive); */
/*   gtk_widget_set_sensitive (editor->priv->secret_dnd_label, sensitive); */
/*   gtk_widget_set_sensitive (editor->priv->secret_expander, sensitive); */
/* } */

/********************************************************
 * Search for a keyring item
 */

typedef struct _SearchKeyringItemCallbackData
{
  guint32     item_id;
  GtkTreeIter iter;
  gboolean    found;
} SearchKeyringItemCallbackData;

static gboolean
search_keyring_item_worker (GtkTreeModel                  *model,
                            GtkTreePath                   *path G_GNUC_UNUSED,
                            GtkTreeIter                   *iter,
                            SearchKeyringItemCallbackData *data)
{
  guint32 item_id;
 
  g_assert (data != NULL);

  gtk_tree_model_get (model, iter, EDITOR_COLUMN_ID, &item_id, -1);

  if (item_id == data->item_id) 
    {
      data->iter = *iter;
      data->found = TRUE;
    }

  return data->found;
}

/**
 * search_keyring_item:
 * @editor: a #GKMKeyringEditor.
 * @item_id: id of the keyring item to look for.
 * @iter: a pointer to a #GtkTreeIter or %NULL.
 * @path: a pointer to a pointer to a #GtkTreePath or %NULL.
 *
 * Looks for @item_id in the keyring_items #GtkTreeModel, and if it is found,
 * fills those of @iter, @path, and @row which are not %NULL so that 
 * they point to it.
 *
 * Returns: %TRUE if @item_id was found; %FALSE otherwise.
 **/

static gboolean
search_keyring_item (GKMKeyringEditor	  *editor,
		     guint32               item_id, 
                     GtkTreeIter          *iter, 
                     GtkTreePath         **path,
                     GtkTreeRowReference **row)
{
  SearchKeyringItemCallbackData data;

  data.item_id = item_id;
  data.found = FALSE;

  gtk_tree_model_foreach (GTK_TREE_MODEL (editor->priv->keyring_items), (GtkTreeModelForeachFunc) search_keyring_item_worker, &data);

  if (data.found)
    {
      if (iter != NULL)
        {
          *iter = data.iter;
        }
      if (path != NULL)
        {
          *path = gtk_tree_model_get_path (GTK_TREE_MODEL (editor->priv->keyring_items), &data.iter);
        }
      if (row != NULL)
        {
          if (path != NULL)
            {
              *row = gtk_tree_row_reference_new (GTK_TREE_MODEL (editor->priv->keyring_items), *path);
            }
          else
            {
              GtkTreePath *tmp_path;

              tmp_path = gtk_tree_model_get_path (GTK_TREE_MODEL (editor->priv->keyring_items), &data.iter);

              *row = gtk_tree_row_reference_new (GTK_TREE_MODEL (editor->priv->keyring_items), tmp_path);
              gtk_tree_path_free (tmp_path);
            }
        }
    }

  return data.found;
}

/********************************************************
 * Update info on the keyring items.
 */

static void
update_keyring_items_worker_callback (GnomeKeyringResult  result, 
                                      GList              *list, 
                                      GKMKeyringEditor   *editor)
{
  GList *tmp;

  g_return_if_fail (GKM_IS_KEYRING_EDITOR (editor));

  if (result != GNOME_KEYRING_RESULT_OK)
    {
      /* FIXME: same as update_keyrings_worker_callback */
      complain_about_gnome_keyring_bad_result (NULL, result);

      return;
    }
       
  gtk_list_store_clear (editor->priv->keyring_items);

  for (tmp = list; tmp != NULL; tmp = tmp->next)
    {
      GtkTreeIter iter;

      gtk_list_store_append (editor->priv->keyring_items, &iter);
      gtk_list_store_set (editor->priv->keyring_items, &iter, EDITOR_COLUMN_ID, tmp->data, -1);

      gkm_keyring_editor_update_keyring_item_info (editor, (guint32) tmp->data, &iter);
    }
}

static void
gkm_keyring_editor_update_keyring_items (GKMKeyringEditor *self)
{
  gnome_keyring_list_item_ids (self->priv->keyring_name,
  			       (GnomeKeyringOperationGetListCallback) 
			       update_keyring_items_worker_callback, 
                               self, NULL);
}
 
/********************************************************
 * Update info on a keyring item.
 */

typedef struct _UpdateKeyringItemCallbackData
{
  GKMKeyringEditor *editor;
  GtkTreeRowReference *row;
} UpdateKeyringItemCallbackData;

static void
update_keyring_item_callback_data_free (UpdateKeyringItemCallbackData *data)
{
  gtk_tree_row_reference_free (data->row);
  g_free (data);
}

static void
update_keyring_item_info_worker_callback (GnomeKeyringResult             result,
                                          GnomeKeyringItemInfo          *info,
                                          UpdateKeyringItemCallbackData *data)
{
  char *value;
  time_t time;
  GtkTreePath *path;
  GtkTreeIter iter;
  GnomeKeyringItemType type;
  
  if (result != GNOME_KEYRING_RESULT_OK) 
    {
      g_warning ("Failed to get keyring info.");

      return;
    }

  if (!gtk_tree_row_reference_valid (data->row))
    {
      g_warning ("A row disappeared while we were waiting for the data...");

      return;
    }

  path = gtk_tree_row_reference_get_path (data->row);
  gtk_tree_model_get_iter (GTK_TREE_MODEL (data->editor->priv->keyring_items), &iter, path);
  gtk_tree_path_free (path);
  
  value = gnome_keyring_item_info_get_display_name (info);
  gtk_list_store_set (data->editor->priv->keyring_items, &iter, EDITOR_COLUMN_NAME, value, -1);
  g_free (value);

  type = gnome_keyring_item_info_get_type (info);
  gtk_list_store_set (data->editor->priv->keyring_items, &iter, EDITOR_COLUMN_TYPE, type, -1);

  value = gnome_keyring_item_info_get_secret (info);
  gtk_list_store_set (data->editor->priv->keyring_items, &iter, EDITOR_COLUMN_SECRET, value, -1);
  g_free (value);

  time = gnome_keyring_item_info_get_ctime (info);
  gtk_list_store_set (data->editor->priv->keyring_items, &iter, EDITOR_COLUMN_CTIME, time, -1);

  time = gnome_keyring_item_info_get_mtime (info);
  gtk_list_store_set (data->editor->priv->keyring_items, &iter, EDITOR_COLUMN_MTIME, time, -1);
}

/**
 * gkm_keyring_editor_update_keyring_item_info:
 * @self: a #GKMKeyringEditor.
 * @item_id: the id of the item whose info is to be updated.
 * @iter: a #GtkTreeIter pointing to @keyring in the keyrings @GtkListStore, 
 * or %NULL.
 *
 * Update the information on @keyring in the keyrings liststore.
 **/

static void
gkm_keyring_editor_update_keyring_item_info (GKMKeyringEditor *self, 
		 			     guint32           item_id,
					     GtkTreeIter      *iter)
{
  UpdateKeyringItemCallbackData *data;

  data = g_new0 (UpdateKeyringItemCallbackData, 1);
  data->editor = self;

  if (iter == NULL)
    {
      if (!search_keyring_item (self, item_id, NULL, NULL, &data->row))
        {
          g_warning ("gkm_keyring_editor_update_keyring_item_info: Tried to update the information of a keyring item we don't know about");
        }
    }
  else
    {
      GtkTreePath *path;

      path = 
	gtk_tree_model_get_path (GTK_TREE_MODEL (self->priv->keyring_items),
				 iter);
      data->row = 
	gtk_tree_row_reference_new (GTK_TREE_MODEL (self->priv->keyring_items),
				    path);
      gtk_tree_path_free (path);
    }
  
  gnome_keyring_item_get_info (self->priv->keyring_name,
  			       item_id,
                               (GnomeKeyringOperationGetItemInfoCallback) 
			       update_keyring_item_info_worker_callback, 
                               data, 
			       (GDestroyNotify) 
			       update_keyring_item_callback_data_free);
}
