/*
   Copyright (C) 2005 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2005 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GKM_NEW_KEYRING_DIALOG_H
#define GKM_NEW_KEYRING_DIALOG_H

#include <config.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* This guy's tiny, but let's present an interface like the rest */
#define GKMCreateKeyringDialog GtkDialog

GtkWidget *gkm_create_keyring_dialog_new (void);

gchar *gkm_create_keyring_dialog_get_name (GKMCreateKeyringDialog *dialog);
gchar *gkm_create_keyring_dialog_get_password (GKMCreateKeyringDialog *dialog);

G_END_DECLS

#endif
