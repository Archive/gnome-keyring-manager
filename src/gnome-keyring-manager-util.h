/*
   Copyright (C) 2004 Fernando Herrera <fherrera@onirica.com>
   Copyright (C) 2004 Mariano Suárez-Alvarez <mariano@gnome.org>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GNOME_KEYRING_MANAGER_UTIL_H
#define GNOME_KEYRING_MANAGER_UTIL_H

#include <config.h>

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <gnome-keyring.h>

G_BEGIN_DECLS

#define LOCKED_ICON   "stock_lock"
#define UNLOCKED_ICON "stock_lock-open"

#define GKM_STOCK_DELETE_KEY     "gkm-stock-delete-key"
#define GKM_STOCK_DELETE_KEYRING "gkm-stock-delete-keyring"
#define GKM_STOCK_NEW_KEY        "gkm-stock-new-key"
#define GKM_STOCK_NEW_KEYRING    "gkm-stock-new-keyring"

#define GLADEFILE "/gnome-keyring-manager.glade"

typedef struct _SetColumnPrefsData
{
  int column_id;
  const char *action_name;
  const char *key_name;
} SetColumnPrefsData;

void error_dialog (GtkWindow  *transient_parent, 
                   const char *fmt, 
                   ...) G_GNUC_PRINTF(2, 3);

void complain_about_gnome_keyring_bad_result (GtkWindow *transient_parent,
                                              GnomeKeyringResult  result);

void gkm_register_icons (void);

char *format_date_for_display (time_t date);

void gkm_show_about_dialog (GtkWindow *transient_parent);

const gchar *gkm_get_application_path (void);

void toggle_view_column_action (GtkToggleAction *toggle_action, 
                                GConfClient     *client);

void update_column_visibility (GConfClient       *client,
                               guint              cnxn_id,
                               GConfEntry        *entry,
                               GtkTreeViewColumn *column);

GSList *set_column_visibility_preferences (const SetColumnPrefsData *toggle,
					   guint		     num_elements,
	 			           const gchar              *dir_name,
				           GtkTreeView              *tree_view,
				           GtkActionGroup           *action_group,
					   GConfClient		    *gconf_client);

void close_gconf_connections (GSList      *connections, 
		              const gchar *dir_name,
			      GConfClient *gconf_client);

void format_date_cell_data_func (GtkTreeViewColumn *column,
                                 GtkCellRenderer   *cell,
                                 GtkTreeModel      *model,
                                 GtkTreeIter       *iter,
                                 gpointer           data);

gint tree_model_compare_strings (GtkTreeModel *model,
				 GtkTreeIter  *a,
				 GtkTreeIter  *b,
				 gpointer      data);

gint tree_model_compare_uints (GtkTreeModel *model,
			       GtkTreeIter  *a,
			       GtkTreeIter  *b,
			       gpointer      data);

gint tree_model_compare_booleans (GtkTreeModel *model,
				  GtkTreeIter  *a,
				  GtkTreeIter  *b,
				  gpointer      data);

G_END_DECLS

#endif
