/* 
   Copyright (C) 2005 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2005 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>

#include <glib/gi18n.h>
#include <glade/glade.h>

#include "gnome-keyring-manager-util.h"

#include "gnome-keyring-manager-create-keyring-dialog.h"

#define GLADE_ROOT "new_keyring_dialog"

typedef struct _GKMCreateKeyringDialogPrivate  GKMCreateKeyringDialogPrivate;

struct _GKMCreateKeyringDialogPrivate
{
  GtkWidget *dialog;
  GtkWidget *keyring_name;
  GtkWidget *password_entry;
  GtkWidget *confirm_entry;
  GtkWidget *create_button;

  gboolean has_name;
  gboolean passwords_match;
};

static void gkm_create_keyring_dialog_connect_signals (GKMCreateKeyringDialogPrivate *priv);

GtkWidget *
gkm_create_keyring_dialog_new ()
{
  GKMCreateKeyringDialogPrivate *priv;
  GladeXML  *xml;

  priv = g_new0 (GKMCreateKeyringDialogPrivate, 1);

  xml = glade_xml_new (GLADEDIR GLADEFILE,
		       GLADE_ROOT, NULL);

  if(!xml) 
    {
      g_warning ("Unable to load the glade file. Your install is broken.");
      exit (1);
    }

  priv->dialog = glade_xml_get_widget (xml, GLADE_ROOT);

  priv->keyring_name = glade_xml_get_widget (xml, "name_entry");
  priv->password_entry = glade_xml_get_widget (xml, "password_entry");
  priv->confirm_entry = glade_xml_get_widget (xml, "confirm_entry");

  priv->create_button = glade_xml_get_widget (xml, "create_button");
  /* FIXME: This should be done in the glade file */
  gtk_widget_set_sensitive (priv->create_button, FALSE);

  priv->has_name = FALSE;
  priv->passwords_match = FALSE;

  g_object_set_data (G_OBJECT (priv->dialog), "ckd-priv", priv);

  g_object_unref (xml);

  gkm_create_keyring_dialog_connect_signals (priv);

  return priv->dialog;
}

gchar *
gkm_create_keyring_dialog_get_name (GKMCreateKeyringDialog *dialog)
{
  GKMCreateKeyringDialogPrivate *priv;  

  priv = g_object_get_data (G_OBJECT (dialog), "ckd-priv");

  return g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->keyring_name)));
}

gchar *
gkm_create_keyring_dialog_get_password (GKMCreateKeyringDialog *dialog)
{
  GKMCreateKeyringDialogPrivate *priv;

  priv = g_object_get_data (G_OBJECT (dialog), "ckd-priv");

  return g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->password_entry)));
}

static void
on_destroy_cb (GtkWidget *dialog G_GNUC_UNUSED,
	       GKMCreateKeyringDialogPrivate *priv)
{
   g_free (priv);
}

static void
on_password_entry_changed_cb (GtkWidget *widget G_GNUC_UNUSED,
			      GKMCreateKeyringDialogPrivate *priv)
{
  const gchar *password_text;
  const gchar *confirm_text;

  password_text = gtk_entry_get_text (GTK_ENTRY (priv->password_entry));
  confirm_text = gtk_entry_get_text (GTK_ENTRY (priv->confirm_entry));

  if (g_str_equal (password_text, confirm_text) && !g_str_equal (password_text, ""))
    {
      priv->passwords_match = TRUE;
    }
  else
    {
      priv->passwords_match = FALSE;
    }

  gtk_widget_set_sensitive (priv->create_button, 
			    priv->passwords_match && priv->has_name);
}

static void
on_name_entry_changed_cb (GtkWidget *widget G_GNUC_UNUSED,
			  GKMCreateKeyringDialogPrivate *priv)
{
  const gchar *name_text;

  name_text = gtk_entry_get_text (GTK_ENTRY (priv->keyring_name));

  if (!g_str_equal (name_text, ""))
    {
      priv->has_name = TRUE;
    }
  else
    {
      priv->has_name = FALSE;
    }

  gtk_widget_set_sensitive (priv->create_button, 
			    priv->passwords_match && priv->has_name);
}

static void
gkm_create_keyring_dialog_connect_signals (GKMCreateKeyringDialogPrivate *priv)
{
  g_signal_connect (G_OBJECT (priv->dialog),
		    "destroy",
		    G_CALLBACK (on_destroy_cb),
		    priv);

  g_signal_connect (G_OBJECT (priv->keyring_name),
		    "changed",
		    G_CALLBACK (on_name_entry_changed_cb),
		    priv);

  g_signal_connect (G_OBJECT (priv->password_entry),
		    "changed",
		    G_CALLBACK (on_password_entry_changed_cb),
		    priv);
  g_signal_connect (G_OBJECT (priv->confirm_entry),
		    "changed",
		    G_CALLBACK (on_password_entry_changed_cb),
		    priv);
}
