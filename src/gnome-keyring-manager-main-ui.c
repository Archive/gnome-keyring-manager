/* 
   Copyright (C) 2005 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2005 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>

#include <glib/gi18n.h>
#include <gtk/gtkmarshal.h>
#include <gtk/gtkmessagedialog.h>
#include <glade/glade.h>
#include <glade/glade-build.h>
#include <libgnome/gnome-help.h>
#include <libgnomeui/gnome-stock-icons.h>

#include "gnome-keyring-manager.h"
#include "gnome-keyring-manager-util.h"
#include "gnome-keyring-manager-acl-display.h"
#include "gnome-keyring-manager-attribute-display.h"
#include "gnome-keyring-manager-create-keyring-dialog.h"
#include "gnome-keyring-manager-keyring-editor.h"
#include "gnome-keyring-manager-keyring-manager.h"

#include "gnome-keyring-manager-main-ui.h"

#define GLADE_ROOT "Keyring Manager"
#define MENU_FILE  "/gnome-keyring-manager-ui.xml"

struct _GKMMainUIPrivate
{
  GtkWidget *window;
  GtkWidget *keyrings_treeview;
  GtkWidget *keys_treeview;
  GtkWidget *locked_notebook;
  GtkWidget *attribute_display;
  GtkWidget *acl_display;
  GtkWidget *sidebar;

  gboolean keyrings_visible;

  GtkUIManager   *ui_manager;
  GtkActionGroup *action_group;

  GObject *manager;
  GObject *editor;
};

static void gkm_main_ui_finalize   (GObject *object);

static void gkm_main_ui_initialize_keyrings_tree_view (GKMMainUI *self);
static void gkm_main_ui_initialize_keys_tree_view     (GKMMainUI *self);

static void gkm_main_ui_keyring_actions_set_sensitive (GKMMainUI *self,
						       gboolean sensitive);
static void gkm_main_ui_key_actions_set_sensitive     (GKMMainUI *self,
						       gboolean sensitive);

static void gkm_main_ui_populate_key_attributes       (GKMMainUI   *self,
						       GtkTreeIter  iter);
static void gkm_main_ui_populate_key_acl              (GKMMainUI   *self,
					               GtkTreeIter  iter);

static void gkm_main_ui_update_title                  (GKMMainUI *self);

static void on_keys_treeview_name_edited              (GtkCellRendererText *renderer,
						       gchar               *path,
						       gchar               *new_text,
						       GKMMainUI           *self);

static void gkm_main_ui_connect_glade_signals         (GKMMainUI *self,
						       GladeXML  *xml);

static void gkm_main_ui_initialize_menu               (GKMMainUI *self,
						       GtkWidget *vbox_main);

G_DEFINE_TYPE (GKMMainUI, gkm_main_ui, G_TYPE_OBJECT);

static gint destroy_signal;

static void
gkm_main_ui_class_init (GKMMainUIClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  gobject_class->finalize = gkm_main_ui_finalize;

  destroy_signal = 
    g_signal_new ("destroy",
		  G_TYPE_FROM_CLASS (gobject_class),
		  G_SIGNAL_RUN_CLEANUP | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  0, NULL, NULL,
		  gtk_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

}

static void
gkm_main_ui_init (GKMMainUI *self)
{
  GladeXML     *xml;
  GtkTreeModel *tree_model;
  gchar *default_keyring;
  GtkWidget *vbox_main;

  self->priv = g_new0 (GKMMainUIPrivate, 1);

  self->priv->keyrings_visible = FALSE;
  glade_register_widget (GKM_TYPE_ATTRIBUTE_DISPLAY, glade_standard_build_widget,
                         NULL, NULL);

  glade_register_widget (GKM_TYPE_ACL_DISPLAY, glade_standard_build_widget,
                         NULL, NULL);

  xml = glade_xml_new (GLADEDIR GLADEFILE,
		       GLADE_ROOT, NULL);

  if(!xml) 
    {
      g_warning ("Unable to load the glade file. Your install is broken.");
      exit (1);
    }

  self->priv->window = glade_xml_get_widget (xml, 
					     GLADE_ROOT);

  self->priv->keyrings_treeview = glade_xml_get_widget (xml,
							"keyrings_treeview");
  self->priv->keys_treeview 	= glade_xml_get_widget (xml,
							"keys_treeview");
  self->priv->attribute_display = glade_xml_get_widget (xml,
							"attributes_display");
  self->priv->acl_display       = glade_xml_get_widget (xml,
							"acl_display");
  self->priv->sidebar           = glade_xml_get_widget (xml,
							"sidebar_vbox");


  vbox_main = glade_xml_get_widget (xml, "vbox_main");
  gkm_main_ui_initialize_menu (self, vbox_main);
  gkm_main_ui_initialize_keyrings_tree_view (self);
  gkm_main_ui_initialize_keys_tree_view (self);

  self->priv->manager = gkm_keyring_manager_new ();
  tree_model = gkm_keyring_manager_get_model (GKM_KEYRING_MANAGER (self->priv->manager));
  gtk_tree_view_set_model (GTK_TREE_VIEW (self->priv->keyrings_treeview), 
			   tree_model);
  /* FIXME: choose the keyring to open by default elsewhere */
  /* or by default just open the first in the list, and allow for another to be 
     chosen through a function call */
  default_keyring = 
    gkm_keyring_manager_get_default_keyring (GKM_KEYRING_MANAGER (self->priv->manager));

  self->priv->editor = gkm_keyring_editor_new (NULL);
  tree_model = gkm_keyring_editor_get_model (GKM_KEYRING_EDITOR (self->priv->editor));
  gtk_tree_view_set_model (GTK_TREE_VIEW (self->priv->keys_treeview), 
			   tree_model);

  g_free (default_keyring);

  gkm_main_ui_connect_glade_signals (self, xml);

  g_object_unref (xml);
}

static void
gkm_main_ui_finalize (GObject *object)
{
  GKMMainUI *self;

  g_return_if_fail (GKM_IS_MAIN_UI (object));

  self = GKM_MAIN_UI (object);

  g_free (self->priv);

  G_OBJECT_CLASS (gkm_main_ui_parent_class)->finalize (object);
}

GObject *
gkm_main_ui_new ()
{
  GKMMainUI *main_ui;

  main_ui = g_object_new (GKM_TYPE_MAIN_UI, NULL);

  gkm_main_ui_update_title (main_ui);

  return G_OBJECT (main_ui);
}

static void
format_timeout_cell_data_func (GtkTreeViewColumn *column G_GNUC_UNUSED,
                               GtkCellRenderer   *cell,
                               GtkTreeModel      *model,
                               GtkTreeIter       *iter,
                               gpointer           data)
{
  gint     column_id;
  guint32  timeout;
  gchar   *readable_timeout;

  column_id = GPOINTER_TO_INT (data);

  gtk_tree_model_get(model, iter, column_id, &timeout, -1); 
  readable_timeout = 
    g_strdup_printf (ngettext ("%d second", "%d seconds", timeout), timeout);
  
  g_object_set(cell, "text", readable_timeout, NULL);

  g_free (readable_timeout);
}

static void
format_keyring_name_cell_data_func (GtkTreeViewColumn *column G_GNUC_UNUSED,
				    GtkCellRenderer   *cell,
				    GtkTreeModel      *model,
				    GtkTreeIter       *iter,
				    gpointer           data)
{
  gint      column_id;
  gchar    *keyring_name;
  gchar    *display_name;
  gboolean  is_default;

  column_id = GPOINTER_TO_INT (data);

  gtk_tree_model_get(model, iter, 
		     column_id, &keyring_name, 
		     MANAGER_COLUMN_DEFAULT, &is_default,
		     -1); 

  if (g_str_equal (keyring_name, "session"))
    {
      g_free (keyring_name);
      keyring_name = g_strdup (_("session"));
    }

  if (is_default)
    {
      display_name = g_strdup_printf ("<span weight='bold'>%s</span>", 
				      keyring_name);
    }
  else
    {
      display_name = g_strdup (keyring_name);
    }

  g_object_set(cell, "markup", display_name, NULL);

  g_free (keyring_name);
  g_free (display_name);
}

static void
gkm_main_ui_initialize_keyrings_tree_view (GKMMainUI *self)
{
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
  
  g_return_if_fail (GKM_IS_MAIN_UI (self));

  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Locked"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keyrings_treeview), 
			       column);
  renderer = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_add_attribute (column, renderer, "stock-id", 
				      MANAGER_COLUMN_LOCK);
  gtk_tree_view_column_set_sort_column_id (column, MANAGER_COLUMN_LOCK);
 
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Keyring"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keyrings_treeview), 
			       column);
  gtk_tree_view_column_set_expand (column, TRUE);
  renderer = gtk_cell_renderer_text_new ();
  g_object_set (G_OBJECT (renderer), "editable", TRUE, NULL);
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_sort_column_id (column, MANAGER_COLUMN_KEYRING);
  gtk_tree_view_column_set_cell_data_func (column,
  				           renderer,
				           (GtkTreeCellDataFunc) format_keyring_name_cell_data_func,
				           GINT_TO_POINTER (MANAGER_COLUMN_KEYRING), 
					   NULL);

  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Lock on Idle"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keyrings_treeview), 
			       column);
  renderer = gtk_cell_renderer_toggle_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_add_attribute (column, renderer, "active", 
				      MANAGER_COLUMN_LOCK_ON_IDLE);
  gtk_tree_view_column_set_sort_column_id (column, MANAGER_COLUMN_LOCK_ON_IDLE);
 
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Lock Timeout"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keyrings_treeview), 
			       column);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func (column,
  				           renderer,
				           (GtkTreeCellDataFunc) format_timeout_cell_data_func,
				           GINT_TO_POINTER (MANAGER_COLUMN_LOCK_TIMEOUT), 
					   NULL);
  gtk_tree_view_column_set_sort_column_id (column, MANAGER_COLUMN_LOCK_TIMEOUT);
 
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Modification Time"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keyrings_treeview), 
			       column);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_visible (column, FALSE);
  gtk_tree_view_column_set_cell_data_func (column,
  				           renderer,
				           (GtkTreeCellDataFunc) format_date_cell_data_func,
				           GINT_TO_POINTER (MANAGER_COLUMN_MTIME), 
					   NULL);
  gtk_tree_view_column_set_sort_column_id (column, MANAGER_COLUMN_MTIME);
 
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Creation Time"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keyrings_treeview), 
			       column);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_visible (column, FALSE);
  gtk_tree_view_column_set_cell_data_func (column,
  				           renderer,
				           (GtkTreeCellDataFunc) format_date_cell_data_func,
				           GINT_TO_POINTER (MANAGER_COLUMN_CTIME), 
					   NULL);
  gtk_tree_view_column_set_sort_column_id (column, MANAGER_COLUMN_CTIME);
}

static void 
key_type_get_pixbuf (GtkTreeViewColumn *column G_GNUC_UNUSED, 
		     GtkCellRenderer *renderer, 
		     GtkTreeModel *model,
		     GtkTreeIter *iter, 
		     gpointer *data G_GNUC_UNUSED)
{
  int         type;
  const char *stock_id;

  gtk_tree_model_get (model, iter, EDITOR_COLUMN_TYPE, &type, -1);
  switch (type)
    {
      case GNOME_KEYRING_ITEM_GENERIC_SECRET:
        stock_id = GNOME_STOCK_AUTHENTICATION;
        break;
      case GNOME_KEYRING_ITEM_NETWORK_PASSWORD:
        stock_id = GTK_STOCK_NETWORK;
        break;
      case GNOME_KEYRING_ITEM_NOTE:
        stock_id = GNOME_STOCK_BOOK_OPEN;
        break;
      default:
        stock_id = GNOME_STOCK_BLANK;
    }
  
  g_object_set (G_OBJECT (renderer), "stock-id", stock_id, NULL);
}

static void
gkm_main_ui_initialize_keys_tree_view (GKMMainUI *self)
{
  GtkTreeViewColumn   *column;
  GtkCellRenderer     *renderer;
  
  g_return_if_fail (GKM_IS_MAIN_UI (self));
  
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Type"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keys_treeview), column);
  renderer = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func (column, renderer, 
					   (GtkTreeCellDataFunc) key_type_get_pixbuf, 
					   NULL, NULL);
  gtk_tree_view_column_set_sort_column_id (column, EDITOR_COLUMN_TYPE);

  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Name"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keys_treeview), column);
  gtk_tree_view_column_set_expand (column, TRUE);
  renderer = gtk_cell_renderer_text_new ();
  g_object_set (G_OBJECT (renderer), "editable", TRUE, NULL);
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_add_attribute (column, renderer, "text", EDITOR_COLUMN_NAME);
  gtk_tree_view_column_set_sort_column_id (column, EDITOR_COLUMN_NAME);
  g_signal_connect (G_OBJECT (renderer),
		    "edited",
		    G_CALLBACK (on_keys_treeview_name_edited),
		    self);


  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Modification Time"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keys_treeview), column);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_visible (column, FALSE);
  gtk_tree_view_column_set_cell_data_func (column,
  				           renderer,
				           (GtkTreeCellDataFunc) format_date_cell_data_func,
				           GINT_TO_POINTER (EDITOR_COLUMN_MTIME), NULL);
  gtk_tree_view_column_set_sort_column_id (column, EDITOR_COLUMN_MTIME);

  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Creation Time"));
  gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->keys_treeview), column);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_visible (column, FALSE);
  gtk_tree_view_column_set_cell_data_func (column,
  				           renderer,
				           (GtkTreeCellDataFunc) format_date_cell_data_func,
				           GINT_TO_POINTER (EDITOR_COLUMN_CTIME), NULL);
  gtk_tree_view_column_set_sort_column_id (column, EDITOR_COLUMN_CTIME);
}

typedef struct _PopulateKeyPropsCBData
{
  GKMMainUI *self;
  GtkTreeIter iter;
} PopulateKeyPropsCBData;

static void
gkm_main_ui_populate_key_attributes_callback (PopulateKeyPropsCBData    *data,
					      GnomeKeyringItemType       item_type,
					      GnomeKeyringAttributeList *attributes)
{
  gchar *secret;
  GtkTreeModel *model;
  GKMAttributeDisplay *display;

  model = 
    gkm_keyring_editor_get_model (GKM_KEYRING_EDITOR (data->self->priv->editor));

  display = GKM_ATTRIBUTE_DISPLAY (data->self->priv->attribute_display);

  gtk_tree_model_get (model, &data->iter, 
		      EDITOR_COLUMN_SECRET, &secret, 
		      -1);

  gkm_attribute_display_set_item_type  (display, item_type);
  gkm_attribute_display_set_attributes (display, attributes);
  gkm_attribute_display_set_secret     (display, secret);

  g_free (secret);
  g_free (data);
}

static void
gkm_main_ui_populate_key_attributes (GKMMainUI   *self,
				     GtkTreeIter  iter)
{
  PopulateKeyPropsCBData *data;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  data = g_new0 (PopulateKeyPropsCBData, 1);
  data->self = self;
  data->iter = iter;

  gkm_keyring_editor_get_attributes_list (GKM_KEYRING_EDITOR (self->priv->editor),
					  iter,
					  (GKMKeyringEditorGetAttributesCallback)
					  gkm_main_ui_populate_key_attributes_callback,
					  data);
}

static void
gkm_main_ui_populate_key_acl_callback (PopulateKeyPropsCBData  *data,
				       GList *acl)
{
  gkm_acl_display_set_acl  (GKM_ACL_DISPLAY (data->self->priv->acl_display), 
			    acl);

  g_free (data);
}

static void
gkm_main_ui_populate_key_acl (GKMMainUI   *self,
			      GtkTreeIter  iter)
{
  PopulateKeyPropsCBData *data;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  data = g_new0 (PopulateKeyPropsCBData, 1);
  data->self = self;
  data->iter = iter;

  gkm_keyring_editor_get_acl (GKM_KEYRING_EDITOR (self->priv->editor),
			      iter,
			      (GKMKeyringEditorGetAclCallback)
			      gkm_main_ui_populate_key_acl_callback,
			      data);
}

static void
gkm_main_ui_update_title (GKMMainUI *self)
{
  gchar *keyring_name;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  keyring_name = 
    gkm_keyring_editor_get_keyring_name (GKM_KEYRING_EDITOR (self->priv->editor));

  if (keyring_name == NULL)
    {
      gtk_window_set_title (GTK_WINDOW (self->priv->window), 
			    _("Keyring Manager"));
    }
  else
    {
      gchar *title;
      
      /* To translators: %s is the name of the keyring. */
      title = g_strdup_printf (_("%s Keyring"), keyring_name);
      gtk_window_set_title (GTK_WINDOW (self->priv->window), title);

      g_free (title);
    }

  g_free (keyring_name);
}

/************************************************************
 * Manually connected signals
 */

static void
on_gkm_window_destroy (GtkWidget *window G_GNUC_UNUSED,
		       GKMMainUI *self)
{
  g_return_if_fail (GKM_IS_MAIN_UI (self));

  g_signal_emit (self, destroy_signal, 0);

  g_object_run_dispose (G_OBJECT (self));
}

/* Manager menu functions */

static void
create_keyring_dialog_cb (GtkDialog *dialog,
			  gint       response_id,
			  GKMMainUI *self)
{
  gchar *name;
  gchar *password;

  switch (response_id)
    {
    case GTK_RESPONSE_DELETE_EVENT:
    case GTK_RESPONSE_CANCEL:
      break;
    case GTK_RESPONSE_ACCEPT:
      name = gkm_create_keyring_dialog_get_name (dialog);
      password = gkm_create_keyring_dialog_get_password (dialog);

      gkm_keyring_manager_create_keyring (GKM_KEYRING_MANAGER (self->priv->manager),
					  name, password);

      g_free (name);
      g_free (password);
      break;
    default:
      g_assert_not_reached ();
    }

  gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
on_new_keyring_activate (GtkWidget *widget G_GNUC_UNUSED,
			 GKMMainUI *self)
{
  GtkWidget *dialog;

  dialog = gkm_create_keyring_dialog_new ();

  g_signal_connect (G_OBJECT (dialog),
		    "response",
		    G_CALLBACK (create_keyring_dialog_cb),
		    self);

  gtk_window_present (GTK_WINDOW (dialog));
}

/* Deleting a key or keyring */

typedef struct _DeleteActionCBData
{
  gpointer    object;
  GtkTreeIter iter;
  void        (*deletion_func)();
} DeleteActionCBData;

static void
delete_action_dialog_cb (GtkDialog          *dialog,
			 gint                response_id,
			 DeleteActionCBData *data)
{
  switch (response_id)
    {
    case GTK_RESPONSE_DELETE_EVENT:
    case GTK_RESPONSE_CANCEL:
      break;
    case GTK_RESPONSE_ACCEPT:
      data->deletion_func (data->object, data->iter);
      break;
    default:
      g_assert_not_reached ();
    }

  g_free (data);
  gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
delete_action_common (GKMMainUI          *self,
		      const gchar        *primary_text,
		      const gchar        *secondary_text,
		      DeleteActionCBData *data,
		      GtkTreeSelection   *selection,
		      gint                column)
{
  GtkWidget    *dialog;
  GtkTreeModel *model;
  gchar        *name;
  
  if (gtk_tree_selection_get_selected (selection, &model, &data->iter))
    {
      gtk_tree_model_get (model, &data->iter, 
			  column, &name, -1);
      
      dialog = gtk_message_dialog_new (GTK_WINDOW (self->priv->window),
				       GTK_DIALOG_DESTROY_WITH_PARENT,
				       GTK_MESSAGE_QUESTION,
				       GTK_BUTTONS_CANCEL,
				       primary_text,
				       name);
      
      gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
						secondary_text);
      
      gtk_dialog_add_buttons (GTK_DIALOG (dialog),
			      GTK_STOCK_DELETE,
			      GTK_RESPONSE_ACCEPT,
			      NULL);
      
      g_signal_connect (G_OBJECT (dialog),
			"response",
			G_CALLBACK (delete_action_dialog_cb),
			data);
      
      gtk_window_present (GTK_WINDOW (dialog));
      
      g_free (name);
    }
  else
    {
      g_free (data);
    }
}

static void
on_delete_key_activate (GtkWidget *widget G_GNUC_UNUSED,
			GKMMainUI *self)
{
  GtkTreeSelection   *selection;
  DeleteActionCBData *data;

  data = g_new0 (DeleteActionCBData, 1);
  data->object = self->priv->editor;
  data->deletion_func = gkm_keyring_editor_delete_key;
  
  selection = 
    gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->keys_treeview));

  delete_action_common (self,
			_("Delete the '%s' key?"),
			_("Deleting a key cannot be undone."),
			data, selection, EDITOR_COLUMN_NAME);
}

static void
on_delete_keyring_activate (GtkWidget *widget G_GNUC_UNUSED,
			    GKMMainUI *self)
{
  GtkTreeSelection    *selection;
  DeleteActionCBData  *data;

  data = g_new0 (DeleteActionCBData, 1);
  data->object = self->priv->manager;
  data->deletion_func = gkm_keyring_manager_delete_keyring;
  
  selection = 
    gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->keyrings_treeview));

  delete_action_common (self,
			_("Delete the '%s' keyring?"),
			_("Deleting a keyring cannot be undone."),
			data, selection, MANAGER_COLUMN_KEYRING);
}

/* End manager menu functions */

/* View menu functions */

static void
on_show_keyrings_activate (GtkWidget *widget G_GNUC_UNUSED,
			   GKMMainUI *self)
{
  GtkAction *action;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  if (!self->priv->keyrings_visible) 
    {
      gtk_widget_show (self->priv->sidebar);
    }
  else 
    {
      gtk_widget_hide (self->priv->sidebar);
    }

  self->priv->keyrings_visible = !self->priv->keyrings_visible;

  action = gtk_action_group_get_action (self->priv->action_group,
					"ShowKeyrings");

  g_signal_handlers_block_by_func (action, on_show_keyrings_activate, self);
  gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action),
				self->priv->keyrings_visible);
  g_signal_handlers_unblock_by_func (action, on_show_keyrings_activate, self);

}

/* End of view menu functions */

/* Help menu functions */

static void
on_help_activate (GtkWidget *widget G_GNUC_UNUSED,
		  GKMMainUI *self)
{
  GError *error;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  error = NULL;
  
  /* TODO: handle the error */
  gnome_help_display ("gnome-keyring-manager", NULL, &error);

  if (error != NULL) {
    g_error ("Unable to load help: %s", error->message);

    g_error_free (error);
  }
}

static void
on_about_activate (GtkWidget *widget G_GNUC_UNUSED,
		   GKMMainUI *self)
{
  g_return_if_fail (GKM_IS_MAIN_UI (self));

  gkm_show_about_dialog (GTK_WINDOW (self->priv->window));
}

/* End of help menu functions */

/* Keyrings treeview functions */

static gboolean
on_keyrings_treeview_button_press_event (GtkWidget         *keyrings_treeview,
					 GdkEventButton    *event,
					 GKMMainUI         *self)
{
  g_return_val_if_fail (GKM_IS_MAIN_UI (self), FALSE);
  
  if (event->type == GDK_BUTTON_PRESS && event->button == 3)
    {
      GtkTreeSelection *selection;
      GtkTreePath *path;
      
      selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (keyrings_treeview));
      
      /* Only bring up the menu if there is a row under the mouse. */
      if (gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW (keyrings_treeview),
					 event->x, event->y,
					 &path, NULL, NULL, NULL))
	{
/* 	  GtkTreeIter iter; */
	  
/* 	  gtk_tree_selection_unselect_all (selection); */
/* 	  gtk_tree_selection_select_path (selection, path); */
/* 	  gtk_tree_model_get_iter (GTK_TREE_MODEL (self->priv->keyrings),  */
/* 				   &iter, path); */
/* 	  gtk_tree_path_free (path); */
	  
/* 	  keyring_tree_popup_menu (keyrings_treeview, &iter, event, self); */
	}
      
      return TRUE;
    }
  else
    {
      return FALSE;
    }
} 

static gboolean
on_keyrings_treeview_popup_menu (GtkWidget *widget G_GNUC_UNUSED,
				 GKMMainUI *self)
{
  g_return_val_if_fail (GKM_IS_MAIN_UI (self), FALSE);

  return FALSE;
}

static void
gkm_main_ui_keyring_actions_set_sensitive (GKMMainUI                *self,
					   gboolean                 sensitive)
{
	GtkAction *action;
	action = gtk_action_group_get_action (self->priv->action_group,
					      "DeleteKeyring");
	gtk_action_set_sensitive (action, sensitive);
}

static void
gkm_main_ui_key_actions_set_sensitive (GKMMainUI                *self,
				       gboolean                 sensitive)
{
	GtkAction *action;
	action = gtk_action_group_get_action (self->priv->action_group,
					      "DeleteKey");
	gtk_action_set_sensitive (action, sensitive);
}

static void
on_keyrings_treeview_selection_changed (GtkTreeSelection *selection,
					GKMMainUI        *self)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gchar *keyring_name;
      
      gtk_tree_model_get (model, &iter, 
			  MANAGER_COLUMN_KEYRING, &keyring_name, -1);
      
      gkm_keyring_editor_set_keyring_name (GKM_KEYRING_EDITOR (self->priv->editor),
					   keyring_name);
      gkm_main_ui_update_title (self);

      gkm_main_ui_keyring_actions_set_sensitive (self, TRUE);
      g_free (keyring_name);
    }
  else 
  {
     gkm_main_ui_keyring_actions_set_sensitive (self, FALSE);
  }
}

static void
on_keyrings_model_row_changed (GtkTreeModel *model,
			       GtkTreePath  *path G_GNUC_UNUSED,
			       GtkTreeIter  *iter,
			       GKMMainUI    *self)
{
  if (gkm_keyring_editor_get_keyring_name (GKM_KEYRING_EDITOR (self->priv->editor)) == NULL)
    {
      gchar *keyring_name;
      
      gtk_tree_model_get (model, iter, 
			  MANAGER_COLUMN_KEYRING, &keyring_name, -1);
      
      gkm_keyring_editor_set_keyring_name (GKM_KEYRING_EDITOR (self->priv->editor),
					   keyring_name);
    }
}


/* End of keyrings treeview functions */

/* Key treeview functions */

static void
on_keys_treeview_name_edited (GtkCellRendererText *renderer G_GNUC_UNUSED,
			      gchar               *path,
			      gchar               *new_text,
			      GKMMainUI           *self)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  model = gtk_tree_view_get_model (GTK_TREE_VIEW (self->priv->keys_treeview));
  gtk_tree_model_get_iter_from_string (model, &iter, path);

  gkm_keyring_editor_set_key_name (GKM_KEYRING_EDITOR (self->priv->editor), 
				   iter, new_text);
}

static void
on_keys_treeview_selection_changed (GtkTreeSelection *selection,
				    GKMMainUI        *self)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gkm_main_ui_populate_key_attributes (self, iter);
      gkm_main_ui_populate_key_acl        (self, iter);
      gkm_main_ui_key_actions_set_sensitive (self, TRUE);
    }
  else
    {
      gkm_main_ui_key_actions_set_sensitive (self, FALSE);
    }
}

/* End of key treeview functions */

/* Attributes display functions */

static void
on_attributes_display_secret_changed (GtkWidget *widget G_GNUC_UNUSED,
				      gchar     *new_secret,
				      GKMMainUI *self)
{
  GtkTreeSelection *selection;
  GtkTreeModel     *model;
  GtkTreeIter       iter;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  selection = 
    gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->keys_treeview));

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gkm_keyring_editor_set_key_secret (GKM_KEYRING_EDITOR (self->priv->editor),
					 iter, new_secret);
    }
}

static void
on_attributes_display_attribute_changed (GtkWidget *widget G_GNUC_UNUSED,
					 GnomeKeyringAttributeList *attributes,
					 GKMMainUI *self)
{
  GtkTreeSelection *selection;
  GtkTreeModel     *model;
  GtkTreeIter       iter;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  selection = 
    gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->keys_treeview));

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gkm_keyring_editor_set_attributes_list (GKM_KEYRING_EDITOR (self->priv->editor),
					      iter, attributes);
    }
}

/* End of attributes display functions */

/* Acl Display functions */

static void
on_acl_display_acl_changed (GtkWidget *widget G_GNUC_UNUSED,
			    GList     *acl,
			    GKMMainUI *self)
{
  GtkTreeSelection *selection;
  GtkTreeModel     *model;
  GtkTreeIter       iter;

  g_return_if_fail (GKM_IS_MAIN_UI (self));

  selection = 
    gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->keys_treeview));

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gkm_keyring_editor_set_acl (GKM_KEYRING_EDITOR (self->priv->editor),
				  iter, acl);
    }
}

/* End of acl display functions */

static void 
gkm_main_ui_connect_glade_signals (GKMMainUI *self,
				   GladeXML  *xml)
{
  GtkWidget        *widget;
  GtkTreeSelection *selection;
  GtkTreeModel     *model;

  g_signal_connect (G_OBJECT (self->priv->window),
		    "destroy",
		    G_CALLBACK (on_gkm_window_destroy),
		    self);

  g_signal_connect (self->priv->keyrings_treeview, 
		    "button-press-event", 
		    G_CALLBACK (on_keyrings_treeview_button_press_event), 
		    self);
  g_signal_connect (self->priv->keyrings_treeview, 
		    "popup-menu", 
		    G_CALLBACK (on_keyrings_treeview_popup_menu), 
		    self);

  g_signal_connect (G_OBJECT (self->priv->attribute_display),
		    "secret-changed",
		    G_CALLBACK (on_attributes_display_secret_changed),
		    self);
  g_signal_connect (G_OBJECT (self->priv->attribute_display),
		    "attribute-changed",
		    G_CALLBACK (on_attributes_display_attribute_changed),
		    self);

  g_signal_connect (G_OBJECT (self->priv->acl_display),
		    "acl-changed",
		    G_CALLBACK (on_acl_display_acl_changed),
		    self);

  widget = glade_xml_get_widget (xml,
				 "sidebar_close_button");
  g_signal_connect (G_OBJECT (widget),
		    "pressed",
		    G_CALLBACK (on_show_keyrings_activate),
		    self);

  selection = 
    gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->keyrings_treeview));
  g_signal_connect (G_OBJECT (selection),
		    "changed",
		    G_CALLBACK (on_keyrings_treeview_selection_changed), 
		    self);

  selection = 
    gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->keys_treeview));
  g_signal_connect (G_OBJECT (selection),
		    "changed",
		    G_CALLBACK (on_keys_treeview_selection_changed), 
		    self);

  model = gtk_tree_view_get_model (GTK_TREE_VIEW (self->priv->keyrings_treeview));
  g_signal_connect (G_OBJECT (model),
		    "row-changed",
		    G_CALLBACK (on_keyrings_model_row_changed), 
		    self);
}

/* Normal items */
static const GtkActionEntry entries[] = {
  { "KeyringMenu", NULL, N_("_Keyring"), NULL, NULL, NULL },
  { "EditMenu",    NULL, N_("_Edit"),    NULL, NULL, NULL },
  { "ViewMenu",    NULL, N_("_View"),    NULL, NULL, NULL },
  { "HelpMenu",    NULL, N_("_Help"),    NULL, NULL, NULL },
 
  { "NewKey",        GKM_STOCK_NEW_KEY, N_("_New Key"), "<control>N", NULL, NULL },
  { "NewKeyring",    GKM_STOCK_NEW_KEYRING, N_("New _Keyring"), "<control><shift>N", NULL, G_CALLBACK (on_new_keyring_activate) },
  { "DeleteKey",     GKM_STOCK_DELETE_KEY, N_("_Delete Key"), NULL, NULL, G_CALLBACK (on_delete_key_activate) },
  { "DeleteKeyring", GKM_STOCK_DELETE_KEYRING, N_("D_elete Keyring"), NULL, NULL, G_CALLBACK (on_delete_keyring_activate) },
  { "Close",         GTK_STOCK_CLOSE, NULL, NULL, NULL, G_CALLBACK (on_gkm_window_destroy) },
 
  { "Undo",  GTK_STOCK_UNDO,  NULL, NULL, NULL, NULL },
  { "Redo",  GTK_STOCK_REDO,  NULL, NULL, NULL, NULL },
  { "Cut",   GTK_STOCK_CUT,   NULL, NULL, NULL, NULL },
  { "Copy",  GTK_STOCK_COPY,  NULL, NULL, NULL, NULL },
  { "Paste", GTK_STOCK_PASTE, NULL, NULL, NULL, NULL },

  { "Help",  GTK_STOCK_HELP,  N_("_Contents"), "F1", NULL, G_CALLBACK (on_help_activate) },
  { "About", GTK_STOCK_ABOUT, NULL, NULL, NULL, G_CALLBACK (on_about_activate) }
};

/* Toggle items */
static const GtkToggleActionEntry toggle_entries[] = {
  { "ShowKeyrings",         NULL, N_("_Keyrings"), "F9", NULL, G_CALLBACK (on_show_keyrings_activate), FALSE},
  { "ShowModificationTime", NULL, N_("Show _Modification Time"), NULL, NULL, NULL, FALSE },
  { "ShowLastAccessedTime", NULL, N_("Show Last _Accessed Time"), NULL, NULL, NULL, FALSE }
};

static void
gkm_main_ui_initialize_menu (GKMMainUI *self, GtkWidget *vbox_main)
{
  GtkWidget *menubar;
  GtkAccelGroup *accel_group;
  GError *error = NULL;

  self->priv->action_group = gtk_action_group_new ("MenuActions"); 
  gtk_action_group_set_translation_domain (self->priv->action_group, GETTEXT_PACKAGE);
  gtk_action_group_add_actions (self->priv->action_group, entries, 
				G_N_ELEMENTS (entries), self);
  gtk_action_group_add_toggle_actions (self->priv->action_group, toggle_entries, 
				       G_N_ELEMENTS (toggle_entries), self);
  self->priv->ui_manager = gtk_ui_manager_new ();
  gtk_ui_manager_insert_action_group (self->priv->ui_manager, 
				      self->priv->action_group, 0);
  accel_group = gtk_ui_manager_get_accel_group (self->priv->ui_manager);
  gtk_window_add_accel_group (GTK_WINDOW (self->priv->window), accel_group);
  if (!gtk_ui_manager_add_ui_from_file (self->priv->ui_manager,
					GLADEDIR MENU_FILE,
					&error))
  {
    g_message (_("building menus failed: %s"), error->message);
    g_error_free (error);
    exit (EXIT_FAILURE);
  }

  menubar = gtk_ui_manager_get_widget (self->priv->ui_manager, 
				       "/KeyringManagerMenu");
  gtk_widget_show (menubar);
  gtk_box_pack_start (GTK_BOX (vbox_main), menubar, FALSE, FALSE, 0);
  gkm_main_ui_key_actions_set_sensitive (self, FALSE);
  gkm_main_ui_keyring_actions_set_sensitive (self, FALSE);
}
