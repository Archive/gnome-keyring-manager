/* 
   Copyright (C) 2004 Fernando Herrera <fherrera@onirica.com>
   Copyright (C) 2004 Mariano Suárez-Alvarez <mariano@gnome.org>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <config.h>

#include <unistd.h>
#include <time.h>
#include <string.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#include <gconf/gconf-client.h>
#include <gtk/gtkaboutdialog.h>
#include <libgnome/libgnome.h>

#include "gnome-keyring-manager.h"
#include "gnome-keyring-manager-util.h"

#define GPL \
 "This program is free software; you can redistribute it and/or modify\n" \
 "it under the terms of the GNU General Public License as published by\n" \
 "the Free Software Foundation; either version 2 of the License, or\n" \
 "(at your option) any later version.\n\n" \
 "This program is distributed in the hope that it will be useful,\n" \
 "but WITHOUT ANY WARRANTY; without even the implied warranty of\n" \
 "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" \
 "GNU General Public License for more details.\n\n" \
 "You should have received a copy of the GNU General Public License\n" \
 "along with this program; if not, write to the Free Software\n" \
 "Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA."

static const char *stock_icons[] =
{
  LOCKED_ICON,
  UNLOCKED_ICON,
};

static struct { 
  gchar *filename;
  gchar *stock_id;
} custom_stock_icons[] = {
  { GLADEDIR "/stock_delete-key.png",     GKM_STOCK_DELETE_KEY },
  { GLADEDIR "/stock_delete-keyring.png", GKM_STOCK_DELETE_KEYRING },
  { GLADEDIR "/stock_new-key.png",        GKM_STOCK_NEW_KEY },    
  { GLADEDIR "/stock_new-keyring.png",    GKM_STOCK_NEW_KEYRING }
};

/********************************************************************
 * Utility functions
 */

void
error_dialog (GtkWindow *transient_parent, const char *fmt, ...)
{
  GtkWidget *dialog;
  char *msg;
  va_list args;

  va_start (args, fmt);
  
  msg = g_strdup_vprintf (fmt, args);

  va_end (args);
  
  dialog = gtk_message_dialog_new (transient_parent,
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_OK,
                                   "%s",
                                   msg);
  g_free (msg);
  
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

void
complain_about_gnome_keyring_bad_result (GtkWindow          *transient_parent,
                                         GnomeKeyringResult  result)
{
  /*
   * FIXME: There are messages missing here...
   */

  switch (result) 
    {
      case GNOME_KEYRING_RESULT_NO_KEYRING_DAEMON:
        error_dialog (GTK_WINDOW (transient_parent), _("Keyring daemon is not running."));
        break;
        
      case GNOME_KEYRING_RESULT_IO_ERROR:
        error_dialog (GTK_WINDOW (transient_parent), _("Cannot communicate with the keyring daemon."));
        break;
        
      case GNOME_KEYRING_RESULT_DENIED:
        error_dialog (GTK_WINDOW (transient_parent), _("Access denied to the keyring."));
        break;

      case GNOME_KEYRING_RESULT_ALREADY_EXISTS:
        error_dialog (GTK_WINDOW (transient_parent), _("Keyring already exists."));
        break;
      
      case GNOME_KEYRING_RESULT_NO_SUCH_KEYRING:
        error_dialog (GTK_WINDOW (transient_parent), _("No such keyring."));
        break;
        
      case GNOME_KEYRING_RESULT_ALREADY_UNLOCKED:
        error_dialog (GTK_WINDOW (transient_parent), _("Keyring already unlocked."));
        break;
      
      case GNOME_KEYRING_RESULT_OK:
        g_warning ("complain_about_gnome_keyring_bad_result called about GNOME_KEYRING_RESULT_OK");
        break;

      default:
        error_dialog (GTK_WINDOW (transient_parent), _("Unknown keyring error."));
    }
}

void
gkm_register_icons (void)
{
  static gboolean been_here = FALSE;

  unsigned int i;
  GtkIconTheme *icon_theme;
  GtkIconSet *icon_set;
  GtkIconFactory *factory;

  if (been_here)
    {
      return;
    }

  icon_theme = gtk_icon_theme_get_default ();
  factory = gtk_icon_factory_new ();

  /* FIXME: maybe handle errors here */
  /* FIXME: these 16s should actually be measured in ems */

  for (i = 0; i < G_N_ELEMENTS (stock_icons); i++)
    {
      GdkPixbuf *icon;

      icon = gtk_icon_theme_load_icon (icon_theme, stock_icons[i], 16, 0, NULL);
      icon_set = gtk_icon_set_new_from_pixbuf (icon);
      gtk_icon_factory_add (factory, stock_icons[i], icon_set);
    }

  for (i = 0; i < G_N_ELEMENTS (custom_stock_icons); i++) 
    {
      GtkIconSource *icon_source;

      icon_set = gtk_icon_set_new ();
      icon_source = gtk_icon_source_new ();
      gtk_icon_source_set_filename (icon_source, 
				    custom_stock_icons[i].filename);
      gtk_icon_set_add_source (icon_set, icon_source);
      gtk_icon_source_free (icon_source);
      gtk_icon_factory_add (factory, custom_stock_icons[i].stock_id, 
			    icon_set);
      gtk_icon_set_unref (icon_set);
    }

  gtk_icon_factory_add_default (factory);

  g_object_unref (factory);

  been_here = TRUE;
}

/**
 * format_date_for_display:
 * @date: a #time_t
 *
 * Returns: @date formatted in a way suitable for display.
 * 
 * This code is taken from evolution/mail/message-list.c
 */

char *
format_date_for_display (time_t date)
{
  time_t nowdate = time(NULL);
  time_t yesdate;
  struct tm then, now, yesterday;
  char buf[26];
  gboolean done = FALSE;

  if (date == 0)
    {
      return g_strdup (_("?"));
    }

  localtime_r (&date, &then);
  localtime_r (&nowdate, &now);
  if (then.tm_mday == now.tm_mday &&
      then.tm_mon == now.tm_mon &&
      then.tm_year == now.tm_year) 
    {
      strftime (buf, 26, _("Today %l:%M %p"), &then);
      done = TRUE;
    }

  if (!done) 
    {
      yesdate = nowdate - 60 * 60 * 24;
      localtime_r (&yesdate, &yesterday);
      if (then.tm_mday == yesterday.tm_mday &&
          then.tm_mon == yesterday.tm_mon &&
          then.tm_year == yesterday.tm_year) 
        {
          strftime (buf, 26, _("Yesterday %l:%M %p"), &then);
          done = TRUE;
        }
    }

  if (!done) 
    {
      int i;
      for (i = 2; i < 7; i++) 
        {
          yesdate = nowdate - 60 * 60 * 24 * i;
          localtime_r (&yesdate, &yesterday);
          if (then.tm_mday == yesterday.tm_mday &&
              then.tm_mon == yesterday.tm_mon &&
              then.tm_year == yesterday.tm_year) 
            {
              strftime (buf, 26, _("%a %l:%M %p"), &then);
              done = TRUE;
              break;
            }
        }
    }

  if (!done) 
    {
      if (then.tm_year == now.tm_year) 
        {
          strftime (buf, 26, _("%b %d %l:%M %p"), &then);
        } 
      else 
        {
            strftime (buf, 26, _("%b %d %Y"), &then);
        }
    }

  return g_strdup (buf);
}

enum 
  {
    LINK_TYPE_EMAIL,
    LINK_TYPE_URL
  };

static void
gkm_about_dialog_activate_links (GtkWidget *about_dialog G_GNUC_UNUSED,
				 const gchar *link,
				 gpointer data)
{
  gchar *new_link;

  switch (GPOINTER_TO_INT (data))
    {
    case LINK_TYPE_EMAIL:
      new_link = g_strdup_printf ("mailto: %s", link);
      break;
    case LINK_TYPE_URL:
      new_link = g_strdup (link);
      break;
    default:
      g_assert_not_reached ();
    }

  if (!gnome_url_show (new_link, NULL)) 
    {
      g_warning ("Unable to follow link %s\n", link);
    }

  g_free (new_link);
}

void
gkm_show_about_dialog (GtkWindow *transient_parent)
{
  const char *authors[] = {
    "Fernando Herrera <fherrera@onirica.com>",
    "Mariano Su\303\241rez-Alvarez <mariano@gnome.org>",
    "James Bowes <bowes@cs.dal.ca>",
    NULL
  };

  g_return_if_fail (GTK_IS_WINDOW (transient_parent));

  /*  const char *documenters[] = {
    NULL
    }; */

  gtk_about_dialog_set_email_hook ((GtkAboutDialogActivateLinkFunc) gkm_about_dialog_activate_links,
				   GINT_TO_POINTER (LINK_TYPE_EMAIL), NULL);

  gtk_about_dialog_set_url_hook ((GtkAboutDialogActivateLinkFunc) gkm_about_dialog_activate_links,
				   GINT_TO_POINTER (LINK_TYPE_URL), NULL);

  gtk_show_about_dialog (transient_parent,
			 "name", _("Keyring Manager"),
			 "version", VERSION,
			 "comments",_("A tool to manage the information stored in your keyrings."),
			 "authors", authors,
			 "copyright", _("Copyright \302\251 2004, 2005 The GNOME Love Project"),
			 "documenters", NULL /* FIXME! */,
			 "license", GPL,
			 "logo-icon-name", "stock_keyring",
			 "translator-credits", _("translator-credits"),
			 "website", "http://www.gnome.org/",
			 /* FIXME: add a "website-label" */
			 NULL);
}

const gchar *
gkm_get_application_path (void)
{
  static gchar *path = NULL;

  if (path == NULL)
    {
#if defined(__linux__)
 	{
	  path = g_file_read_link ("/proc/self/exe", NULL);
	}     
#elif defined(__FreeBSD__)
	{
	  path = g_file_read_link ("/proc/curproc/file", NULL);
	}
#else
#warning "gkm_get_application_path() not implemented for this OS!"
#endif
    }

  return path;
}

/********************************************************************
 * Toggle display of columns
 */

void
toggle_view_column_action (GtkToggleAction *toggle_action, 
                           GConfClient	   *gconf_client)
{
  gboolean old_state, state;
  char *key;
  GError *error;

  state = gtk_toggle_action_get_active (toggle_action);
  key = g_object_get_data (G_OBJECT (toggle_action), "key");

  error = NULL;
  old_state = gconf_client_get_bool (gconf_client, key, &error);
  if (error != NULL)
    {
      g_warning ("Error while trying to read the gconf key %s: %s", key, error->message);
      g_error_free (error);
    }
 
  if (state == old_state)
    {
      /* Avoid an infinite loop */
      return;
    }

  /* FIXME: This key might be read-only... In that case we need to call the
   * gconf callback ourselves. Sigh.
   */
  error = NULL;
  gconf_client_set_bool (gconf_client, key, state, &error);
  if (error != NULL)
    {
      g_warning ("Error while setting the gconf key %s: %s", key, error->message);
      g_error_free (error);
    }
}

void
update_column_visibility (GConfClient       *client G_GNUC_UNUSED,
                          guint              cnxn_id G_GNUC_UNUSED,
                          GConfEntry        *entry,
                          GtkTreeViewColumn *column)
{
  GConfValue *value;
  gboolean state;
  GtkToggleAction *action;

  g_return_if_fail (GTK_IS_TREE_VIEW_COLUMN (column));

  value = gconf_entry_get_value (entry);
  state = gconf_value_get_bool (value);
  action = g_object_get_data (G_OBJECT (column), "action");

  gtk_tree_view_column_set_visible (column, state);
  gtk_toggle_action_set_active (action, state);
}

GSList *
set_column_visibility_preferences (const SetColumnPrefsData *toggle,
				   guint		     num_elements,
				   const gchar              *dir_name,
				   GtkTreeView              *tree_view,
				   GtkActionGroup           *action_group,
				   GConfClient		    *gconf_client)
{
  unsigned int i;
  GtkAction *action;
  GtkTreeViewColumn *column;
  char *dir;
  GError *error;
  GSList *gconf_cnxn_ids;

  dir = gconf_concat_dir_and_key (dir_name, "columns");

  error = NULL;
  gconf_client_add_dir (gconf_client, dir, GCONF_CLIENT_PRELOAD_ONELEVEL, &error); 
  if (error != NULL)
    {
      g_warning ("Error trying to add %s to the list of gconf directories we listen to: %s", dir, error->message);
      g_error_free (error);
    }

  gconf_cnxn_ids = NULL;

  for (i = 0; i < num_elements; i++)
    {
      char *key;
      gboolean state;
      int id;

      column = gtk_tree_view_get_column ((tree_view), toggle[i].column_id);
      action = gtk_action_group_get_action (action_group, toggle[i].action_name);

      key = gconf_concat_dir_and_key (dir, toggle[i].key_name);
      g_object_set_data_full (G_OBJECT (action), "key", key, g_free);

      g_object_set_data (G_OBJECT (column), "action", action);

      error = NULL;
      state = gconf_client_get_bool (gconf_client, key, &error); 
      if (error != NULL)
        {
          g_warning ("Error while trying to read the gconf key %s: %s", key, error->message);
          g_error_free (error);
        }

      gtk_tree_view_column_set_visible (column, state);
      gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), state);

      error = NULL;
      id = gconf_client_notify_add (gconf_client, key, (GConfClientNotifyFunc) update_column_visibility, column, NULL, &error);
      if (error != NULL)
        {
          g_warning ("Error while trying to add a listener for changes on the gconf key %s: %s", key, error->message);
          g_error_free (error);
        }

      gconf_cnxn_ids = g_slist_prepend (gconf_cnxn_ids, GINT_TO_POINTER (id));
    }

  g_free (dir);

  return gconf_cnxn_ids;
}

static void
disconnect_gconf_listener (gpointer data,
                           GConfClient *gconf_client)
{
  gconf_client_notify_remove (gconf_client, GPOINTER_TO_INT (data));
}

void
close_gconf_connections (GSList      *connections, 
	                 const gchar *dir_name,
			 GConfClient *gconf_client)
{
  char   *dir;
  GError *error;

  g_slist_foreach (connections, (GFunc) disconnect_gconf_listener, gconf_client);
  g_slist_free (connections);

  dir = gconf_concat_dir_and_key (dir_name, "columns");
  error = NULL;
  gconf_client_remove_dir (gconf_client, dir, &error);
  if (error != NULL)
    {
      g_printerr (_("Error while trying to remove the %s directory from the list of gconf directories we listen to: %s"), 
                  dir, error->message);
      g_error_free (error);
    }
  g_free (dir);
}

void 
format_date_cell_data_func (GtkTreeViewColumn *column G_GNUC_UNUSED,
                            GtkCellRenderer   *cell,
                            GtkTreeModel      *model,
                            GtkTreeIter       *iter,
                            gpointer           data)
{
  gint column_id;
  guint32 time;
  gchar *readable_time;

  column_id = GPOINTER_TO_INT (data);

  gtk_tree_model_get(model, iter, column_id, &time, -1); 
  readable_time = format_date_for_display (time);
  
  g_object_set(cell, "text", readable_time, NULL);

  g_free (readable_time);
}

/***************************************************
 * Functions for sorting the tree views
 */

gint
tree_model_compare_strings (GtkTreeModel *model,
			    GtkTreeIter  *a,
			    GtkTreeIter  *b,
			    gpointer      data)
{
  gint   column;
  gint   ret;
  gchar *string1;
  gchar *string2;

  column = GPOINTER_TO_INT (data);

  gtk_tree_model_get (model, a, column, &string1, -1);
  gtk_tree_model_get (model, b, column, &string2, -1);

  ret = g_utf8_collate (string1, string2);

  g_free (string1);
  g_free (string2);

  return ret;
}

gint
tree_model_compare_uints (GtkTreeModel *model,
			  GtkTreeIter  *a,
			  GtkTreeIter  *b,
			  gpointer      data)
{
  gint  column;
  guint number1;
  guint number2;

  column = GPOINTER_TO_INT (data);

  gtk_tree_model_get (model, a, column, &number1, -1);
  gtk_tree_model_get (model, b, column, &number2, -1);

  if (number1 != number2)
    {
      return (number1 > number2) ? 1 : -1;
    }
  else
    {
      return 0;
    }
}

gint
tree_model_compare_booleans (GtkTreeModel *model,
			     GtkTreeIter  *a,
			     GtkTreeIter  *b,
			     gpointer      data)
{
  gint     column;
  gboolean boolean1;
  gboolean boolean2;

  column = GPOINTER_TO_INT (data);

  gtk_tree_model_get (model, a, column, &boolean1, -1);
  gtk_tree_model_get (model, b, column, &boolean2, -1);

  if (boolean1 != boolean2)
    {
      return (boolean1 == TRUE) ? 1 : -1;
    }
  else
    {
      return 0;
    }
}
