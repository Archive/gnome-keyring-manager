/*
   Copyright (C) 2004 Fernando Herrera <fherrera@onirica.com>
   Copyright (C) 2004 Mariano Suárez-Alvarez <mariano@gnome.org>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GNOME_KEYRING_MANAGER_H
#define GNOME_KEYRING_MANAGER_H

#define GNOME_KEYRING_MANAGER_GCONF_PREFIX "/apps/gnome-keyring-manager"

#include <gtk/gtk.h>

G_BEGIN_DECLS

void gkm_application_init (void);
void gkm_application_open_keyring_editor_for (const char *keyring, GtkWindow *transient_parent);
void gkm_application_open_keyring_manager (void);
void gkm_application_quit (void);

G_END_DECLS


#endif
