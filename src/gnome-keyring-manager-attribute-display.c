/* 
   Copyright (C) 2004, 2005 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2004, 2005 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gnome-keyring.h>
#include <glade/glade.h>

#include "gnome-keyring-manager-util.h"
#include "gnome-keyring-manager-attribute-display.h"

#define GLADE_ROOT "attributes-display_notebook"

struct _GKMAttributeDisplayPrivate
{
  GnomeKeyringItemType item_type;
  GnomeKeyringAttributeList *attributes;
  gint *attribute_mapping;

  gboolean secret_visible;
  gchar *note_secret;

  GtkToggleButton *password_checkbutton;
  GtkToggleButton *s_password_checkbutton;
  GtkToggleButton *note_checkbutton;

  GtkWidget **attribute_entries;
  GtkWidget  *attributes_book;
};

enum
{
  USER_ENTRY = 0,
  DOMAIN_ENTRY,
  SERVER_ENTRY,
  OBJECT_ENTRY,
  METHOD_ENTRY,
  PROTOCOL_ENTRY,
  PORT_ENTRY,
  PASSWORD_ENTRY,
  S_PASSWORD_ENTRY,
  NOTE_ENTRY,
  NUM_ENTRIES
};
#define NON_PASSWORD_ENTRIES_MAX (NUM_ENTRIES - (NUM_ENTRIES - PASSWORD_ENTRY))

enum
{
  NETWORK_PAGE = 0,
  SECRET_PAGE,
  NOTE_PAGE,
  BLANK_PAGE,
  NUM_PAGES
};

enum {
  SECRET_CHANGED,
  ATTRIBUTE_CHANGED,
  LAST_SIGNAL
};

static guint attribute_display_signals[LAST_SIGNAL] = { 0 };

static gboolean   network_password_update_page (GKMAttributeDisplay *display);

static void gkm_attribute_display_clear (GKMAttributeDisplay *display);

static void gkm_attribute_display_show_password_toggle (GtkWidget           *widget,
							GKMAttributeDisplay *self);

static void gkm_attribute_display_finalize   (GObject *object);
static void gkm_attribute_display_destroy    (GtkObject *object);

static void on_attribute_entry_changed (GtkWidget           *widget,
					GKMAttributeDisplay *self);
static void on_secret_changed_callback (GtkWidget           *widget,
					GKMAttributeDisplay *self);

G_DEFINE_TYPE (GKMAttributeDisplay, gkm_attribute_display, GTK_TYPE_VBOX);

static void
gkm_attribute_display_class_init (GKMAttributeDisplayClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);

  gobject_class->finalize = gkm_attribute_display_finalize;

  object_class->destroy = gkm_attribute_display_destroy;

  attribute_display_signals[SECRET_CHANGED] =
    g_signal_new ("secret-changed",
		  G_OBJECT_CLASS_TYPE (gobject_class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GKMAttributeDisplayClass, secret_changed),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__STRING,
		  G_TYPE_NONE, 1,
		  G_TYPE_STRING);

  attribute_display_signals[ATTRIBUTE_CHANGED] =
    g_signal_new ("attribute-changed",
		  G_OBJECT_CLASS_TYPE (gobject_class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GKMAttributeDisplayClass, attribute_changed),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1,
		  G_TYPE_POINTER);
}

static void
gkm_attribute_display_init (GKMAttributeDisplay *self)
{
  GtkWidget     *widget;
  GladeXML      *xml;
  GtkTextBuffer *buffer;
  gint           i;

  self->priv = g_new0 (GKMAttributeDisplayPrivate, 1);

  self->priv->attribute_entries = g_new0 (GtkWidget *, NUM_ENTRIES);

  xml = glade_xml_new (GLADEDIR GLADEFILE,
		       GLADE_ROOT, NULL);

  if(!xml) 
    {
      g_warning ("Unable to load the glade file. Your install is broken.");
      exit (1);
    }

  self->priv->attributes = gnome_keyring_attribute_list_new ();

  self->priv->attribute_mapping = g_new0 (gint, NON_PASSWORD_ENTRIES_MAX);
  memset (self->priv->attribute_mapping, -1,
      sizeof (gint) * NON_PASSWORD_ENTRIES_MAX);

  self->priv->secret_visible = FALSE;

  widget = glade_xml_get_widget (xml, GLADE_ROOT);

  gtk_container_add (GTK_CONTAINER (self), widget);

  gtk_widget_show_all (GTK_WIDGET (self));

  widget = glade_xml_get_widget (xml, GLADE_ROOT);
  self->priv->attributes_book = widget;
  
  /* Get refs for the attribute entries */

  widget = glade_xml_get_widget (xml, "server_entry");
  self->priv->attribute_entries[SERVER_ENTRY] = widget;

  widget = glade_xml_get_widget (xml, "username_entry");
  self->priv->attribute_entries[USER_ENTRY] = widget;

  widget = glade_xml_get_widget (xml, "port_spinbutton");
  self->priv->attribute_entries[PORT_ENTRY] = widget;
  g_signal_connect (widget,
		    "value-changed",
		    G_CALLBACK (on_attribute_entry_changed),
		    self);

  widget = glade_xml_get_widget (xml, "protocol_entry");
  self->priv->attribute_entries[PROTOCOL_ENTRY] = widget;

  widget = glade_xml_get_widget (xml, "method_entry");
  self->priv->attribute_entries[METHOD_ENTRY] = widget;

  widget = glade_xml_get_widget (xml, "domain_entry");
  self->priv->attribute_entries[DOMAIN_ENTRY] = widget;

  widget = glade_xml_get_widget (xml, "object_entry");
  self->priv->attribute_entries[OBJECT_ENTRY] = widget;

  /* Connect the signal handlers for changed attributes (excluding PORT) */
  for (i = 0; i < NUM_ENTRIES - 4; i++)
    {
      g_signal_connect (self->priv->attribute_entries[i],
			"changed",
			G_CALLBACK (on_attribute_entry_changed),
			self);
    }


  widget = glade_xml_get_widget (xml, "password_entry");
  self->priv->attribute_entries[PASSWORD_ENTRY] = widget;
  g_signal_connect (widget,
		    "changed",
		    G_CALLBACK (on_secret_changed_callback),
		    self);

  widget = glade_xml_get_widget (xml, "s_password_entry");
  self->priv->attribute_entries[S_PASSWORD_ENTRY] = widget;
  g_signal_connect (widget,
		    "changed",
		    G_CALLBACK (on_secret_changed_callback),
		    self);

  widget = glade_xml_get_widget (xml, "note_textview");
  self->priv->attribute_entries[NOTE_ENTRY] = widget;
  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
  g_signal_connect (buffer,
		    "changed",
		    G_CALLBACK (on_secret_changed_callback),
		    self);


  widget = glade_xml_get_widget (xml, "password_checkbutton");
  self->priv->password_checkbutton = GTK_TOGGLE_BUTTON (widget);
  g_signal_connect (widget,
		    "toggled",
		    G_CALLBACK (gkm_attribute_display_show_password_toggle),
		    self);

  widget = glade_xml_get_widget (xml, "s_password_checkbutton");
  self->priv->s_password_checkbutton = GTK_TOGGLE_BUTTON (widget);
  g_signal_connect (widget,
		    "toggled",
		    G_CALLBACK (gkm_attribute_display_show_password_toggle),
		    self);

  widget = glade_xml_get_widget (xml, "note_checkbutton");
  self->priv->note_checkbutton = GTK_TOGGLE_BUTTON (widget);
  g_signal_connect (widget,
		    "toggled",
		    G_CALLBACK (gkm_attribute_display_show_password_toggle),
		    self);


  /* Set to the blank page, so we're not showing empty fields */
  gtk_notebook_set_current_page (GTK_NOTEBOOK (self->priv->attributes_book), 
				 BLANK_PAGE);

  g_object_unref (xml);
}

static void
gkm_attribute_display_finalize (GObject *object)
{
  GKMAttributeDisplay *display;

  g_return_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (object));

  display = GKM_ATTRIBUTE_DISPLAY (object);

  gnome_keyring_attribute_list_free (display->priv->attributes);

  g_free (display->priv->attribute_mapping);

  if (!display->priv->secret_visible)
    {
      g_free (display->priv->note_secret);
    }

  g_free (display->priv);

  G_OBJECT_CLASS (gkm_attribute_display_parent_class)->finalize (object);
}

static void
gkm_attribute_display_destroy (GtkObject *object)
{
  GKMAttributeDisplay *display;

  g_return_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (object));

  display = GKM_ATTRIBUTE_DISPLAY (object);

  GTK_OBJECT_CLASS (gkm_attribute_display_parent_class)->destroy 
    (GTK_OBJECT (display));
}

GtkWidget *
gkm_attribute_display_new (void)
{
  GKMAttributeDisplay *display;

  display = g_object_new (GKM_TYPE_ATTRIBUTE_DISPLAY, NULL);

  return GTK_WIDGET (display);
}

/**************************************************
 * Network passwords
 */

static gboolean
network_password_update_page (GKMAttributeDisplay *self)
{
  guint32 i;
  gboolean non_standard_attributes;
 
  for (i = 0; i < NON_PASSWORD_ENTRIES_MAX; i++)
    {
      g_signal_handlers_block_by_func (self->priv->attribute_entries[i],
				       on_attribute_entry_changed,
				       self);
    }

  /* Reset attribute mappings */
  memset (self->priv->attribute_mapping, -1,
      sizeof (gint) * NON_PASSWORD_ENTRIES_MAX);

  non_standard_attributes = FALSE;
  for (i = 0; i < self->priv->attributes->len; i++)
    {
      GnomeKeyringAttribute attribute;
      gint map_index;
      
      attribute = gnome_keyring_attribute_list_index(self->priv->attributes, i);
      map_index = -1;

      if (strcmp (attribute.name, "user") == 0 
	  && attribute.type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING)
	{
	  map_index = USER_ENTRY;
	  gtk_entry_set_text (GTK_ENTRY (self->priv->attribute_entries[map_index]), 
			      attribute.value.string);
	}
      else if (strcmp (attribute.name, "domain") == 0 
	       && attribute.type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING)
	{
	  map_index = DOMAIN_ENTRY;
	  gtk_entry_set_text (GTK_ENTRY (self->priv->attribute_entries[map_index]),
			      attribute.value.string);
	}
      else if (strcmp (attribute.name, "server") == 0 
	       && attribute.type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING)
	{
	  map_index = SERVER_ENTRY;
	  gtk_entry_set_text (GTK_ENTRY (self->priv->attribute_entries[map_index]),
			      attribute.value.string);
	}
      else if (strcmp (attribute.name, "object") == 0 
	       && attribute.type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING)
	{
	  map_index = OBJECT_ENTRY;
	  gtk_entry_set_text (GTK_ENTRY (self->priv->attribute_entries[map_index]),
			      attribute.value.string);
	}
      else if (strcmp (attribute.name, "authtype") == 0 
	       && attribute.type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING)
	{
	  map_index = METHOD_ENTRY;
	  gtk_entry_set_text (GTK_ENTRY (self->priv->attribute_entries[map_index]),
			      attribute.value.string);
	}
      else if (strcmp (attribute.name, "protocol") == 0 
	       && attribute.type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING)
	{
	  map_index = PROTOCOL_ENTRY;
	  gtk_entry_set_text (GTK_ENTRY (self->priv->attribute_entries[map_index]), 
			      attribute.value.string);
	}
      else if (strcmp (attribute.name, "port") == 0 
	       && attribute.type == GNOME_KEYRING_ATTRIBUTE_TYPE_UINT32)
	{
	  map_index = PORT_ENTRY;
 	  gtk_spin_button_set_value (GTK_SPIN_BUTTON (self->priv->attribute_entries[map_index]), 
				     attribute.value.integer);
	}
      else
        {
          non_standard_attributes = TRUE;
	}

      if (map_index != -1)
	{
	  self->priv->attribute_mapping[map_index] = i;
	  gtk_widget_set_sensitive (self->priv->attribute_entries[map_index], TRUE);
	}
    }

  for (i = 0; i < NON_PASSWORD_ENTRIES_MAX; i++)
    {
      g_signal_handlers_unblock_by_func (self->priv->attribute_entries[i],
					 on_attribute_entry_changed,
					 self);
      /* If a default attribute isn't in the attribute list, disable its widget */
      if (self->priv->attribute_mapping[i] == -1)
        gtk_widget_set_sensitive (self->priv->attribute_entries[i], FALSE);
    }

  return non_standard_attributes;
}

static void
update_attribute_display (GKMAttributeDisplay *self)
{
  int page;

  g_return_if_fail (self->priv->attributes != NULL);
  
  gkm_attribute_display_clear (self);

  switch (self->priv->item_type)
    {
      case GNOME_KEYRING_ITEM_GENERIC_SECRET:
        page = SECRET_PAGE;
        break;
      case GNOME_KEYRING_ITEM_NETWORK_PASSWORD:
        network_password_update_page (self);
        page = NETWORK_PAGE;
        break;
      case GNOME_KEYRING_ITEM_NOTE:
        page = NOTE_PAGE;
	break;
      default:
	page = BLANK_PAGE;
    }

  gtk_notebook_set_current_page (GTK_NOTEBOOK (self->priv->attributes_book), 
				 page);
}

/**************************************************
 * Get and set the type
 */

static void
gkm_attribute_display_clear (GKMAttributeDisplay *self)
{
  int i;

  i = 0;

  /* Don't clear out the passwords */
  for (i = 0; i < NON_PASSWORD_ENTRIES_MAX; i++)
    {
      g_signal_handlers_block_by_func (self->priv->attribute_entries[i],
				       on_attribute_entry_changed,
				       self);
      gtk_entry_set_text (GTK_ENTRY (self->priv->attribute_entries[i]), "");
      g_signal_handlers_unblock_by_func (self->priv->attribute_entries[i],
					 on_attribute_entry_changed,
					 self);
    }
}

void 
gkm_attribute_display_set_secret (GKMAttributeDisplay *self, 
				  const gchar         *secret)
{
  GtkWidget *widget;

  g_return_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (self));

  if (self->priv->item_type == GNOME_KEYRING_ITEM_NOTE)
    {
      GtkTextBuffer *buffer;
      gchar *buffer_text;

      widget = self->priv->attribute_entries[NOTE_ENTRY];
      buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));

      if (!self->priv->secret_visible) 
	{
	  g_free (self->priv->note_secret);
	  self->priv->note_secret = g_strdup (secret);

	  buffer_text = g_strnfill (strlen (secret), '*');
	}
      else
	{
	  buffer_text = g_strdup (secret);
	}
      
      g_signal_handlers_block_by_func (buffer, on_secret_changed_callback, 
				       self);
      gtk_text_buffer_set_text (buffer, buffer_text, -1);
      g_signal_handlers_unblock_by_func (buffer, on_secret_changed_callback, 
					 self);
      
      g_free (buffer_text);
    }
  else
    {
      if (self->priv->item_type == GNOME_KEYRING_ITEM_GENERIC_SECRET)
	{
	  widget =  self->priv->attribute_entries[S_PASSWORD_ENTRY];
	}
      else
	{
	  widget =  self->priv->attribute_entries[PASSWORD_ENTRY];
	}
      g_signal_handlers_block_by_func (widget, on_secret_changed_callback, 
				       self);
      gtk_entry_set_text (GTK_ENTRY (widget), secret);
      g_signal_handlers_unblock_by_func (widget, on_secret_changed_callback, 
					 self);
    }
  
  update_attribute_display (self);
}

gchar *
gkm_attribute_display_get_secret (GKMAttributeDisplay *self)
{
  GtkWidget *widget;
  gchar *secret;

  g_return_val_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (self), NULL);

  if (self->priv->item_type == GNOME_KEYRING_ITEM_NOTE)
    {
      if (self->priv->secret_visible)
	{
	  GtkTextBuffer *buffer;
	  GtkTextIter start;
	  GtkTextIter end;
	  
	  widget = self->priv->attribute_entries[NOTE_ENTRY];
	  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	  
	  gtk_text_buffer_get_start_iter (buffer, &start);
	  gtk_text_buffer_get_end_iter (buffer, &end);
	  
	  secret = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
	}
      else
	{
	  secret = g_strdup (self->priv->note_secret);
	}
    }
  else
    {
      if (self->priv->item_type == GNOME_KEYRING_ITEM_GENERIC_SECRET)
	{
	  widget =  self->priv->attribute_entries[S_PASSWORD_ENTRY];
	}
      else
	{
	  widget =  self->priv->attribute_entries[PASSWORD_ENTRY];
	}
      
      secret = g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));      
    }

  return secret;
}

void 
gkm_attribute_display_set_item_type (GKMAttributeDisplay  *self, 
				     GnomeKeyringItemType  item_type)
{
  g_return_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (self));

  self->priv->item_type = item_type;
  
  update_attribute_display (self);
}

GnomeKeyringItemType 
gkm_attribute_display_get_item_type (GKMAttributeDisplay *self)
{
  g_return_val_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (self), 
			GNOME_KEYRING_ITEM_NO_TYPE);

  return self->priv->item_type;
}

void
gkm_attribute_display_set_attributes (GKMAttributeDisplay       *self,
				      GnomeKeyringAttributeList *attributes)
{
  g_return_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (self));


  gnome_keyring_attribute_list_free (self->priv->attributes);
  self->priv->attributes = gnome_keyring_attribute_list_copy (attributes);

  update_attribute_display (self);
}

GnomeKeyringAttributeList *
gkm_attribute_display_get_attributes (GKMAttributeDisplay *self)
{
  g_return_val_if_fail (GKM_IS_ATTRIBUTE_DISPLAY (self), NULL);

  return gnome_keyring_attribute_list_copy (self->priv->attributes);
}

static void
on_attribute_entry_changed (GtkWidget           *widget G_GNUC_UNUSED,
			    GKMAttributeDisplay *self)
{
  GnomeKeyringAttributeList *attributes;
  gint i;

  /* Find which entry changed, and update it in the attribute list. */
  for (i = 0; i < NON_PASSWORD_ENTRIES_MAX; i++)
    {
      GnomeKeyringAttribute *attribute;
      gint map_index;

      if (widget != self->priv->attribute_entries[i])
	continue;

      map_index = self->priv->attribute_mapping[i];
      if (map_index == -1)
	continue;

      attribute = &gnome_keyring_attribute_list_index (self->priv->attributes,
	  map_index);

      if (attribute->type == GNOME_KEYRING_ATTRIBUTE_TYPE_STRING)
	{
	  g_free (attribute->value.string);

	  attribute->value.string = 
	      g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));
	}
      else
	{
	  attribute->value.integer = 
	      gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (widget));
	}

	break;
    }

  attributes = gkm_attribute_display_get_attributes (self);
  g_signal_emit (self,
		 attribute_display_signals[ATTRIBUTE_CHANGED],
		 0, attributes);
}

static void
swap_note_secret (GKMAttributeDisplay *self)
{
  GtkWidget     *widget;
  GtkTextBuffer *buffer;
  gchar         *tmp;
  
  widget = self->priv->attribute_entries[NOTE_ENTRY];
  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
 
  if (self->priv->secret_visible)
    {
      
      tmp = self->priv->note_secret;
    }
  else
    {
      GtkTextIter    start;
      GtkTextIter    end;

      gtk_text_buffer_get_start_iter (buffer, &start);
      gtk_text_buffer_get_end_iter (buffer, &end);
      
      self->priv->note_secret = 
	gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

      tmp = g_strnfill (strlen (self->priv->note_secret), '*');
    }

  if(tmp)
    {
      g_signal_handlers_block_by_func (buffer, on_secret_changed_callback, 
					self);
      gtk_text_buffer_set_text (buffer, tmp, -1);
      g_signal_handlers_unblock_by_func (buffer, on_secret_changed_callback, 
     					self);
      g_free (tmp);
    }
}

static void
gkm_attribute_display_show_password_toggle (GtkWidget           *widget G_GNUC_UNUSED,
					    GKMAttributeDisplay *self)
{
  self->priv->secret_visible = !self->priv->secret_visible;

  /* We don't want to emit a signal, just change the state of the toggle buttons. */
  self->priv->password_checkbutton->active = self->priv->secret_visible;
  self->priv->s_password_checkbutton->active = self->priv->secret_visible;
  self->priv->note_checkbutton->active = self->priv->secret_visible;

  gtk_entry_set_visibility (GTK_ENTRY (self->priv->attribute_entries[PASSWORD_ENTRY]),
			   self->priv->secret_visible);
  gtk_entry_set_visibility (GTK_ENTRY (self->priv->attribute_entries[S_PASSWORD_ENTRY]),
			   self->priv->secret_visible);
  swap_note_secret (self);

  /* Don't let a user change the password if its not visible (they won't know what
     they're changing). */
  gtk_entry_set_editable (GTK_ENTRY (self->priv->attribute_entries[PASSWORD_ENTRY]),
			  self->priv->secret_visible);
  gtk_entry_set_editable (GTK_ENTRY (self->priv->attribute_entries[S_PASSWORD_ENTRY]),
			  self->priv->secret_visible);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (self->priv->attribute_entries[NOTE_ENTRY]),
			  self->priv->secret_visible);
}

static void
on_secret_changed_callback (GtkWidget           *widget G_GNUC_UNUSED,
			    GKMAttributeDisplay *self)
{
  gchar *secret;

  secret = gkm_attribute_display_get_secret (self);

  g_signal_emit (self,
		 attribute_display_signals[SECRET_CHANGED],
		 0, secret);
}

/* Glade custom widget init function */
GtkWidget *
gkm_attribute_display_glade_new (gchar *widget_name G_GNUC_UNUSED, 
				 gchar *string1     G_GNUC_UNUSED, 
				 gchar *string2     G_GNUC_UNUSED,
				 gint   int1        G_GNUC_UNUSED, 
				 gint   int2        G_GNUC_UNUSED)
{
  return gkm_attribute_display_new ();
}
