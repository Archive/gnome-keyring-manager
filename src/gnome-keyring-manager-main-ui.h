/*
   Copyright (C) 2005 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2005 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GKM_MAIN_UI_H
#define GKM_MAIN_UI_H

#include <config.h>

#include <glib-object.h>

G_BEGIN_DECLS

#define GKM_TYPE_MAIN_UI            (gkm_main_ui_get_type ())
#define GKM_MAIN_UI(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GKM_TYPE_MAIN_UI, GKMMainUI))
#define GKM_MAIN_UI_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GKM_TYPE_MAIN_UI, GKMMainUIClass))
#define GKM_IS_MAIN_UI(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GKM_TYPE_MAIN_UI))
#define GKM_IS_MAIN_UI_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GKM_TYPE_MAIN_UI))
#define GKM_MAIN_UI_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GKM_TYPE_MAIN_UI, GKMMainUIClass))

typedef struct _GKMMainUI         GKMMainUI;
typedef struct _GKMMainUIClass    GKMMainUIClass;
typedef struct _GKMMainUIPrivate  GKMMainUIPrivate;

struct _GKMMainUI
{
  GObject parent;

  GKMMainUIPrivate *priv;
};

struct _GKMMainUIClass
{
  GObjectClass parent;
};

G_GNUC_CONST GType gkm_main_ui_get_type (void);

GObject *gkm_main_ui_new (void);

G_END_DECLS

#endif
