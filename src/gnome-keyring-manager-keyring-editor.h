/*
   Copyright (C) 2004 James Bowes <bowes@cs.dal.ca>
   Copyright (C) 2004 GNOME Love Project <gnome-love@gnome.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef GNOME_KEYRING_MANAGER_KEYRING_EDITOR_H
#define GNOME_KEYRING_MANAGER_KEYRING_EDITOR_H

#include <config.h>

#include <glib-object.h>

G_BEGIN_DECLS

enum
{
  EDITOR_COLUMN_NAME = 0,
  EDITOR_COLUMN_TYPE,
  EDITOR_COLUMN_MTIME,
  EDITOR_COLUMN_CTIME,
  EDITOR_COLUMN_ID,
  EDITOR_COLUMN_SECRET,
  NUM_EDITOR_COLUMNS
} GKMKeyringEditorColumns;

#define GKM_TYPE_KEYRING_EDITOR            (gkm_keyring_editor_get_type ())
#define GKM_KEYRING_EDITOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GKM_TYPE_KEYRING_EDITOR, GKMKeyringEditor))
#define GKM_KEYRING_EDITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GKM_TYPE_KEYRING_EDITOR, GKMKeyringEditorClass))
#define GKM_IS_KEYRING_EDITOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GKM_TYPE_KEYRING_EDITOR))
#define GKM_IS_KEYRING_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GKM_TYPE_KEYRING_EDITOR))
#define GKM_KEYRING_EDITOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GKM_TYPE_KEYRING_EDITOR, GKMKeyringEditorClass))

typedef struct _GKMKeyringEditor         GKMKeyringEditor;
typedef struct _GKMKeyringEditorClass    GKMKeyringEditorClass;
typedef struct _GKMKeyringEditorPrivate  GKMKeyringEditorPrivate;

struct _GKMKeyringEditor
{
  GObject parent;

  GKMKeyringEditorPrivate *priv;
};

struct _GKMKeyringEditorClass
{
  GObjectClass parent;
};

typedef void (*GKMKeyringEditorGetAttributesCallback) (gpointer                   user_data,
						       GnomeKeyringItemType       item_type,
						       GnomeKeyringAttributeList *attributes);

typedef void (*GKMKeyringEditorGetAclCallback) (gpointer  user_data,
						GSList   *acl);

G_GNUC_CONST GType gkm_keyring_editor_get_type (void);

GObject *gkm_keyring_editor_new (const gchar *keyring_name);

GtkTreeModel *gkm_keyring_editor_get_model (GKMKeyringEditor *self);

void gkm_keyring_editor_set_keyring_name   (GKMKeyringEditor *self, 
					    const gchar      *keyring_name);
gchar *gkm_keyring_editor_get_keyring_name (GKMKeyringEditor *self);


void gkm_keyring_editor_set_key_name        (GKMKeyringEditor *self,
				             GtkTreeIter       iter,
				             const gchar      *new_name);

void gkm_keyring_editor_set_key_secret      (GKMKeyringEditor *self,
				             GtkTreeIter       iter,
				             const gchar      *new_secret);

void gkm_keyring_editor_get_attributes_list (GKMKeyringEditor *self,
					     GtkTreeIter       iter,
					     GKMKeyringEditorGetAttributesCallback callback,
					     gpointer          user_data);

void gkm_keyring_editor_set_attributes_list (GKMKeyringEditor *self,
				             GtkTreeIter       iter,
				             GnomeKeyringAttributeList *attributes);

void gkm_keyring_editor_get_acl (GKMKeyringEditor *self,
				 GtkTreeIter       iter,
				 GKMKeyringEditorGetAclCallback callback,
				 gpointer          user_data);

void gkm_keyring_editor_set_acl (GKMKeyringEditor *self,
				 GtkTreeIter       iter,
				 GList            *acl);

void gkm_keyring_editor_delete_key          (GKMKeyringEditor *self,
			                     GtkTreeIter       iter);


G_END_DECLS

#endif
